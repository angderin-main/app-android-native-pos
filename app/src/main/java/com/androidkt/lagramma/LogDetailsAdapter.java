package com.androidkt.lagramma;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Derin Anggara on 4/6/2018.
 */

public class LogDetailsAdapter extends ArrayAdapter<Log> {

    private TextView logDate, logUser, logBody;

    ArrayList<Log> data;

    public LogDetailsAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<Log> objects) {
        super(context, resource, objects);
        this.data = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;

        if(view == null){
            LayoutInflater layoutInflater;
            layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(R.layout.row_item_log_detail,null);
            logDate = (TextView) view.findViewById(R.id.log_date);
//            logRole = (TextView) view.findViewById(R.id.log_role);
            logUser = (TextView) view.findViewById(R.id.log_user);
            logBody = (TextView) view.findViewById(R.id.log_body);
        }

        Log log = data.get(position);

        if(log != null){
            logDate.setText(log.getDate());
//            logRole.setText(log.getRole());
            logUser.setText(log.getUser());
            logBody.setText(log.getBody());
        }

        if (position%2 == 0){
            view.setBackgroundColor(getContext().getResources().getColor(R.color.colorLightGreen));
        }else{
            view.setBackgroundColor(getContext().getResources().getColor(R.color.colorLightDarkGreen));
        }

        return view;
    }

    @Override
    public int getViewTypeCount() {
        if(getCount() < 1){
            return 1;
        }
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
