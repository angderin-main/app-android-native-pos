package com.androidkt.lagramma.ViewPagerAdapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Derin Anggara on 12/22/2017.
 */

public class CustomerViewPagerAdapter extends FragmentPagerAdapter {
    public CustomerViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 : return null;
            case 1 : return null;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
