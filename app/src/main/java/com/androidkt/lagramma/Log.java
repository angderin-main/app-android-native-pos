package com.androidkt.lagramma;

/**
 * Created by Derin Anggara on 4/6/2018.
 */

public class Log {
    private int _id;
    private String date, user, body;

    public Log(int id, String date, String user, String body) {
        _id = id;
        this.date = date;
        this.user = user;
        this.body = body;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
