package com.androidkt.lagramma;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class MainMenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        new PrivilegeTask().execute();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("LAGRAMMA", MODE_PRIVATE);
        Toast.makeText(this, pref.getString("privilege_id",null), Toast.LENGTH_SHORT).show();
        if(pref.getString("privilege_id",null).equals("4")) {
            navigationView.inflateMenu(R.menu.activity_main_menu_drawer);
            //Toast.makeText(this, "MASUK"+pref.getString("privilege_id",null), Toast.LENGTH_SHORT).show();
        }
        else {
            navigationView.inflateMenu(R.menu.menu_no_admin);
            //Toast.makeText(this, "GAK MASUK"+pref.getString("privilege_id",null), Toast.LENGTH_SHORT).show();
        }
        //onNavigationItemSelected(navigationView.getMenu().getItem(0).setChecked(true));
        if(savedInstanceState == null){
            onNavigationItemSelected(navigationView.getMenu().getItem(0).setChecked(true));
            setTitle("Cashier");
            CashierFragment cashierFragment = new CashierFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, cashierFragment).commit();
//            if(pref.getString("privilege_id",null).equals("3")) {
//                onNavigationItemSelected(navigationView.getMenu().getItem(0).setChecked(true));
//                setTitle("Cashier");
//                CashierFragment cashierFragment = new CashierFragment();
//                FragmentManager fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().replace(R.id.fragment, cashierFragment).commit();
//            }else{
//                onNavigationItemSelected(navigationView.getMenu().getItem(1).setChecked(true));
//                setTitle("Transactions");
//                TransactionsFragment transactionsFragment = new TransactionsFragment();
//                FragmentManager fragmentManager = getSupportFragmentManager();
//                fragmentManager.beginTransaction().replace(R.id.fragment, transactionsFragment).commit();
//            }
        }
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_cashier) {
            setTitle("Cashier");
            CashierFragment cashierFragment = new CashierFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, cashierFragment).commit();
        } else if (id == R.id.nav_transactions) {
            setTitle("Transactions");
            TransactionsFragment transactionsFragment = new TransactionsFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, transactionsFragment).commit();
        } else if (id == R.id.nav_stocks) {
            setTitle("Stocks");
            StocksFragment stocksFragment = new StocksFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, stocksFragment).commit();
        } else if (id == R.id.nav_customers) {
            setTitle("Customers");
            CustomersFragment customersFragment = new CustomersFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, customersFragment).commit();
        } else if (id == R.id.nav_users) {
            setTitle("Users");
            UsersFragment usersFragment = new UsersFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, usersFragment).commit();
        }
        else if(id == R.id.nav_log) {
            setTitle("Log");
            LogFragment logFragment = new LogFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, logFragment).commit();
        }
        else if(id == R.id.nav_settings){
            setTitle("Users");
            SettingFragment settingFragment = new SettingFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment, settingFragment).commit();
        }
        else if (id == R.id.nav_logout) {
            SharedPreferences preferences = getApplicationContext().getSharedPreferences("LAGRAMMA",MODE_PRIVATE);
            preferences.edit().remove("username").apply();
            preferences.edit().remove("privilege_id").apply();
            preferences.edit().remove("user_email").apply();
            startActivity(new Intent(MainMenuActivity.this, LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class PrivilegeTask extends AsyncTask<String, Void, Boolean> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainMenuActivity.this);
            progressDialog.setMessage("Get Data..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(MainMenuActivity.this, "GetPrivilege Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            Boolean res = true;
            WebService webService = null;
            try {
                webService = new WebService("getPrivilege", "POST", "");
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            List<Option> privilegeList = new ArrayList<>();
            try {
                JSONObject obj = new JSONObject(jsonString);
                String success = obj.getString("status");
                if(success.equals("1")) {
                    String privilege = obj.getString("message");
                    JSONArray privilegeArray = new JSONArray(privilege);
                    for (int i = 0; i < privilegeArray.length(); i++) {
                        JSONObject privilegeObject = privilegeArray.getJSONObject(i);
                        int privilege_id = privilegeObject.getInt("privilege_id");
                        String privilege_name = privilegeObject.getString("privilege_name");
                        privilegeList.add(new Option(privilege_id,privilege_name));
                    }
                    Global.PrivilegeList = privilegeList;
                }else{
                    res = false;
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            WebService webService2 = null;
            try {
                webService2 = new WebService("getItemCategory", "POST", "token=token");
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString2 = webService2.responseBody;
            List<Option> itemCategoryList = new ArrayList<>();
            try{
                JSONObject obj = new JSONObject(jsonString2);
                String success = obj.getString("status");
                if(success.equals("1")){
                    String itemCategory = obj.getString("message");
                    JSONArray itemCatArray = new JSONArray(itemCategory);
                    for(int i = 0; i < itemCatArray.length(); i++){
                        JSONObject itemCatObject = itemCatArray.getJSONObject(i);
                        int item_category_id = itemCatObject.getInt("item_category_id");
                        String item_category_name = itemCatObject.getString("item_category_name");
                        itemCategoryList.add(new Option(item_category_id, item_category_name));
                    }
                    Global.ItemCategoryList = itemCategoryList;
                }else{
                    res = false;
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            WebService cashTypeService = null;
            try {
                cashTypeService = new WebService("getCashType","POST","token=token");
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString3 = cashTypeService.responseBody;
            List<Option> cashTypeList = new ArrayList<>();
            try{
                JSONObject obj = new JSONObject(jsonString3);
                String success = obj.getString("status");
                if(success.equals("1")){
                    String message = obj.getString("message");
                    JSONArray jsonArray = new JSONArray(message);
                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int id = jsonObject.getInt("cash_type_id");
                        String name = jsonObject.getString("cash_type_name");
                        Log.v("CASH TYPE",name);
                        cashTypeList.add(new Option(id,name));
                    }
                    Global.CashTypeList = cashTypeList;
                }else{
                    res = false;
                }
            }catch(Exception e){
                e.printStackTrace();
            }

            WebService orderStatusService = null;
            try {
                orderStatusService = new WebService("getOrderStatus","POST","token=token");
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString4 = orderStatusService.responseBody;
            List<Option> orderStatusList = new ArrayList<>();
            try{
                JSONObject obj = new JSONObject(jsonString4);
                String success = obj.getString("status");
                if(success.equals("1")){
                    String message = obj.getString("message");
                    JSONArray jsonArray = new JSONArray(message);
                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int id = jsonObject.getInt("order_status_id");
                        String name = jsonObject.getString("order_status_name");
                        Log.v("ORDER STATUS",name);
                        orderStatusList.add(new Option(id,name));
                    }
                    Global.OrderStatusList = orderStatusList;
                }else{
                    res = false;
                }
            }catch(Exception e){
                e.printStackTrace();
            }

            WebService paymentStatusService = null;
            try {
                paymentStatusService = new WebService("getPaymentStatus","POST","token=token");
            } catch (SocketTimeoutException e) {
                e.printStackTrace();

            }
            String jsonString5 = paymentStatusService.responseBody;
            List<Option> paymentStatusList = new ArrayList<>();
            try{
                JSONObject obj = new JSONObject(jsonString5);
                String success = obj.getString("status");
                if(success.equals("1")){
                    String message = obj.getString("message");
                    JSONArray jsonArray = new JSONArray(message);
                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int id = jsonObject.getInt("payment_status_id");
                        String name = jsonObject.getString("payment_status_name");
                        Log.v("PAYMENT STATUS",name);
                        paymentStatusList.add(new Option(id,name));
                    }
                    Global.PaymentStatusList = paymentStatusList;
                }else{
                    res = false;
                }
            }catch(Exception e){
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            progressDialog.hide();
            progressDialog.dismiss();
            if(result){
                Toast.makeText(MainMenuActivity.this, "Get Data Success", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(MainMenuActivity.this, "Please Try Login Again, Cannot Get Data", Toast.LENGTH_LONG).show();
            }
        }
    }
}
