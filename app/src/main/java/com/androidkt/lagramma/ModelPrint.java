package com.androidkt.lagramma;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

/**
 * Created by aldo_ on 10/03/2018.
 */

public class ModelPrint {
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;
    private OutputStream mmOutputStream;
    private InputStream mmInputStream;
    private Thread workerThread;
    private byte[] readBuffer;
    private int readBufferPosition;
    private volatile boolean stopWorker;

    private final int MAX_CHAR = 31;
    private String PRINTER_NAME = "58Printer";

    private int bayar;
    private String cashier;
    private Customers customer;
    private Date dueDate;
    private String paymentType;
    private int diskon;

    private ArrayList<ModelPembayaran> daftarPrints;

    public ModelPrint(){

    }

    public void addDataPrint(Transaction transaction, ArrayList<ModelPembayaran> daftarPrints, String cashier, Customers customer){
        this.bayar = transaction.getCashPaid();
        this.daftarPrints = daftarPrints;
        this.cashier = cashier;
        this.customer = customer;
        this.dueDate = transaction.getOrderDueDate();
        this.paymentType = transaction.getPaymentType();
        this.diskon = transaction.getDiskon();
    }

    public void addDataPrint(){
        this.bayar = bayar;
        this.daftarPrints = daftarPrints;
        this.cashier = cashier;
        this.customer = customer;
        this.dueDate = dueDate;
        this.paymentType = paymentType;
        this.diskon = diskon;
    }

    public void connectPrinter(String printerName){
        try{
            this.PRINTER_NAME = printerName;
            this.findBT();
            this.openBT();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void print(){
        try{
            //Log.v(">>>>>>>> LOG PRINT",this.paymentType);
            this.sendData();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void disconnectPrinter(){
        try{
            this.closeBT();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void findBT() {
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(mBluetoothAdapter == null) {
                //device gk support
            }
            if(!mBluetoothAdapter.isEnabled()) {
                //pls enable bluetooth
            }
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if(pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device.getName().equals(PRINTER_NAME)) {
                        mmDevice = device;
                        break;
                    }
                }
            }
            //FOUND
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void openBT() throws IOException {
        try {
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();
            this.beginListenForData();
            //ok
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void beginListenForData() {
        try {
            final Handler handler = new Handler();
            final byte delimiter = 10;
            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];
            workerThread = new Thread(new Runnable() {
                public void run() {
                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                        try {
                            int bytesAvailable = mmInputStream.available();
                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;
                                        handler.post(new Runnable() {
                                            public void run() {
                                                //myLabel.setText(data);
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }
                        } catch (IOException ex) {
                            stopWorker = true;
                        }
                    }
                }
            });
            workerThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void sendData() throws IOException {
        try {
            // the text typed by the user
            //String msg = myTextbox.getText().toString();

            //Base Setting
            byte[] left = new byte[]{ 0x1b, 0x61, 0x00 };
            byte[] center = new byte[]{ 0x1b, 0x61, 0x01 };
            byte[] right = new byte[]{ 0x1b, 0x61, 0x02 };
            byte[] normal = new byte[]{0x1B,0x21,0x00};
            byte[] bold = new byte[]{0x1B,0x21,0x08};
            byte[] boldmedium = new byte[]{0x1B,0x21,0x20};
            byte[] boldlarge = new byte[]{0x1B,0x21,0x10};

            //Print Header
            mmOutputStream.write(center);
            mmOutputStream.write(boldlarge);
            String header = "LA GRAMMA\n\n";
            mmOutputStream.write(header.getBytes());
            mmOutputStream.write(normal);
            String address = "Jl. Gajahmada 188C\nPontianak, Kalimantan Barat\n-------------------------------\n";
            mmOutputStream.write(address.getBytes());

            //Print Date & Due Date
            mmOutputStream.write(left);
            String currTime = "Order date :\n";
            mmOutputStream.write(currTime.getBytes());
            Date currentTime = Calendar.getInstance().getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy        HH:mm:ss");
            String time = dateFormat.format(currentTime)+"\n";
            mmOutputStream.write(time.getBytes());

            String dueTime = "Due date   :\n";
            mmOutputStream.write(dueTime.getBytes());
            SimpleDateFormat dueFormat = new SimpleDateFormat("EEE, dd MMM yyyy        HH:mm:ss");
            String timedue = dueFormat.format(this.dueDate)+"\n";
            mmOutputStream.write(timedue.getBytes());

            String custName = "\n"+this.customer.getName();
            mmOutputStream.write(custName.getBytes());
            String custPrio = (this.customer.isPriority() ? " - GP" : "")+"\n";
            mmOutputStream.write(custPrio.getBytes());

            String custAddr = this.customer.getAddress()+"\n";
            mmOutputStream.write(custAddr.getBytes());

            String custPhone = this.customer.getPhone();
            int mx = MAX_CHAR-custPhone.length()-this.paymentType.length();
            for(int x=0;x<=mx;x++) custPhone+= " ";
            custPhone += this.paymentType.toUpperCase();
            custPhone += "\n\n";
            mmOutputStream.write(bold);
            mmOutputStream.write(custPhone.getBytes());

            //Print Menu
            int totalAkhir = 0;
            mmOutputStream.write(left);
            mmOutputStream.write(normal);
            for (ModelPembayaran dp: this.daftarPrints) {
                String atas = dp.get_jumlah()+"x "+dp.get_hargasatuan();
                int totPrice = dp.get_jumlah()*dp.get_hargasatuan();
                totalAkhir += totPrice;
                String temp = String.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).format(totPrice));
                int i = MAX_CHAR-atas.length()-temp.length()-1;
                for (int x = 0; x<i; x++) atas+=" ";
                String printText = atas + "Rp" + temp + "\n" +dp.get_nama() + "\n\n";
                mmOutputStream.write(printText.getBytes());
            }

            //Print Total
            mmOutputStream.write(bold);
            String tot = "TOTAL    = ";
            String totharga = String.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).format(totalAkhir));
            int i = MAX_CHAR-tot.length()-totharga.length()-1;
            for(int x=0;x<i;x++) tot+= " ";
            String printTotal = tot + "Rp" + totharga + "\n";
            mmOutputStream.write(printTotal.getBytes());

            //Print Cash
            String cash = "PAYMENT  = ";
            String totcash = String.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).format(this.bayar));
            int j = MAX_CHAR-cash.length()-totcash.length()-1;
            for(int x=0;x<j;x++) cash+= " ";
            String cashTotal = cash + "Rp" + totcash + "\n";
            mmOutputStream.write(cashTotal.getBytes());

            //Print Discount
            if (this.diskon>0) {
                String disc = "DISCOUNT = ";
                String totDisc = String.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).format(this.diskon));
                int dx = MAX_CHAR - disc.length() - totDisc.length() - 1;
                for (int x = 0; x < dx; x++) disc += " ";
                String discountPrint = disc + "Rp" + totDisc + "\n";
                mmOutputStream.write(discountPrint.getBytes());
            }

            //Print Change
            int finalCash = this.bayar-totalAkhir+this.diskon;
            if(finalCash>=0){
                String change = "CHANGE   = ";
                String totchange = String.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).format(finalCash));
                int k = MAX_CHAR-change.length()-totchange.length()-1;
                for(int x=0;x<k;x++) change+= " ";
                String changeTotal = change + "Rp" + totchange + "\n\n";
                mmOutputStream.write(changeTotal.getBytes());
            }
            else{
                String change = "REMAINS  = ";
                finalCash = finalCash - 2*finalCash;
                String totchange = String.valueOf(NumberFormat.getNumberInstance(Locale.getDefault()).format(finalCash));
                int k = MAX_CHAR-change.length()-totchange.length()-1;
                for(int x=0;x<k;x++) change+= " ";
                String changeTotal = change + "Rp" + totchange + "\n\n";
                mmOutputStream.write(changeTotal.getBytes());
            }

            mmOutputStream.write(right);
            mmOutputStream.write(normal);
            String cashierName = "Collected By "+this.cashier+"\n-------------------------------\n";
            mmOutputStream.write(cashierName.getBytes());

            //Print Footer
            mmOutputStream.write(center);
            mmOutputStream.write(normal);
            String thanks = "Sharing Tradition\nwww.lagrammahomemade.com";
            thanks+="\n\n\n\n";
            mmOutputStream.write(thanks.getBytes());

            // tell the user data were sent
            //berhasil

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            //close
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
