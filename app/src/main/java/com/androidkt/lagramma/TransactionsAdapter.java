package com.androidkt.lagramma;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tommy on 23/12/2017.
 */

public class TransactionsAdapter extends ArrayAdapter<Transaction>{

    Context context;
    int layoutResourceId;
    private ArrayList<Transaction> data;


    TextView textOrderId;
    TextView textOrderName;
    TextView textOrderStatus;
    TextView textOrderDate;
    TextView textOrderTime;
    TextView textOrderTotal;

    public TransactionsAdapter(Context context, int layoutResourceId, ArrayList<Transaction> data){
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        if(row == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            row = inflater.inflate(R.layout.row_item_orders, null);

            textOrderId = (TextView) row.findViewById(R.id.text_order_id);
            textOrderName = (TextView) row.findViewById(R.id.text_order_name);
            textOrderStatus = (TextView) row.findViewById(R.id.text_order_status);
            textOrderDate = (TextView) row.findViewById(R.id.text_order_date);
            textOrderTime = (TextView) row.findViewById(R.id.text_order_time);
            textOrderTotal = (TextView) row.findViewById(R.id.text_order_total);

            Transaction transaction = data.get(position);
            if(transaction != null){
                Date date = transaction.getOrderDate();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d MMM yyyy");
                SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm");
                DecimalFormat decimalFormat = new DecimalFormat("#,###");

                textOrderId.setText("Order ID : "+Integer.toString(transaction.getId()));
                textOrderName.setText(transaction.getCustomerName());
                textOrderStatus.setText(transaction.getOrderStatus().toUpperCase());
                textOrderDate.setText(simpleDateFormat.format(date));
                textOrderTime.setText(simpleTimeFormat.format(date));
                double totalPrice = 0;
                for(Item item : transaction.getListItem()){
                    double subTotal = (double)(item.getItemPrice() * item.getItemStocks());
                    totalPrice = totalPrice + subTotal;
                }
                textOrderTotal.setText(decimalFormat.format(totalPrice));
            }
        }
        if(position%2 == 0){
            row.setBackgroundColor(Color.parseColor("#E8F5E9"));
        }else{
            row.setBackgroundColor(Color.parseColor("#D8EED9"));
        }
        return row;
    }

    @Override
    public int getViewTypeCount() {
        if(getCount() < 1){
            return 1;
        }
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
