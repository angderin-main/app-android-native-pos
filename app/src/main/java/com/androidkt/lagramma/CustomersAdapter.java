package com.androidkt.lagramma;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tommy on 20/12/2017.
 */

public class CustomersAdapter extends ArrayAdapter<Customers> {

    Context context;
    int layoutResourceId;
    private ArrayList<Customers> data;

    TextView txtCustomerName;
    TextView txtCustomerPhone;
    TextView txtCustomerAddress;
    TextView txtCustomerPriority;
    TextView txtCustomerPriorNumber;
    TextView txtCustomerCredit;

    public CustomersAdapter(Context context, int layoutResourceId, ArrayList<Customers> data){
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        if(row == null){
            LayoutInflater infalter = LayoutInflater.from(getContext());
            row = infalter.inflate(R.layout.row_item_customers, null);

            txtCustomerName = (TextView) row.findViewById(R.id.text_customer_name);
            txtCustomerPhone = (TextView) row.findViewById(R.id.text_customer_phone);
            txtCustomerAddress = (TextView) row.findViewById(R.id.text_customer_address);
            txtCustomerPriority = (TextView) row.findViewById(R.id.text_customer_priority);
            txtCustomerPriorNumber = (TextView) row.findViewById(R.id.text_customer_priority_number);
            txtCustomerCredit = (TextView) row.findViewById(R.id.text_customer_credit);
        }

        Customers customers = data.get(position);
        if(customers != null){
            txtCustomerName.setText(customers.getName());
            txtCustomerPhone.setText(customers.getPhone());
            txtCustomerAddress.setText(customers.getAddress());
            if(customers.isPriority()){
                txtCustomerPriority.setText("GOLD PASS");
                txtCustomerPriorNumber.setText(Integer.toString(customers.getId()));
            }
            if(customers.getCredit() > 0){
                txtCustomerCredit.setText(Integer.toString(customers.getCredit()));
            }
        }
        if(position%2 == 0){
            row.setBackgroundColor(Color.parseColor("#E8F5E9"));
        }else{
            row.setBackgroundColor(Color.parseColor("#D8EED9"));
        }
        return row;
    }

    public void refreshCustomers(ArrayList<Customers> list){
        if(data != null) {
            data.clear();
            data = new ArrayList<Customers>();
            data.addAll(list);
        }else{
            data = list;
        }
        notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount() {
        if(getCount() < 1){
            return 1;
        }
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
