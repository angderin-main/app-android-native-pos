package com.androidkt.lagramma;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class StocksFragment extends Fragment {

    LinearLayout linearLayout;
    ListView listView;
    Button buttonAdd, buttonUpdate, buttonAll, buttonNastar, buttonLapis, buttonPack, buttonOther, buttonAdd1, buttonAdd10, buttonMin1, buttonMin10;
    EditText textName, textPrice, textGoldCredit, textStock;
    Spinner spinnerCategory;
    SwipeRefreshLayout swipeRefreshLayout;

    SharedPreferences pref;

    StocksAdapter adapter;
    Context context;
    Item item;
    ArrayList<Item> list = new ArrayList<Item>();
    ArrayList<Item> viewList;
    int selectedId, selectedIdx;
    String itemFilter = "";

    public StocksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stocks, container, false);
        context = getContext();

        listView = (ListView) view.findViewById(R.id.list_stocks);
        linearLayout = (LinearLayout) view.findViewById(R.id.linear_stock_detail);
        buttonAdd = (Button) view.findViewById(R.id.btn_add_item);
        buttonUpdate = (Button) view.findViewById(R.id.btn_update_item);
        textName = (EditText) view.findViewById(R.id.text_edit_stock_name);
        textPrice = (EditText) view.findViewById(R.id.text_edit_stock_price);
        textGoldCredit = (EditText) view.findViewById(R.id.text_edit_stock_price2);
        textStock = (EditText) view.findViewById(R.id.text_edit_stock_stock);
        spinnerCategory = (Spinner) view.findViewById(R.id.spinner_edit_item_category);
        buttonAll = (Button) view.findViewById(R.id.btn_filter_stock_all);
        buttonNastar = (Button) view.findViewById(R.id.btn_filter_stock_nastar);
        buttonLapis = (Button) view.findViewById(R.id.btn_filter_stock_lapis);
        buttonPack = (Button) view.findViewById(R.id.btn_filter_stock_pack);
        buttonOther = (Button) view.findViewById(R.id.btn_filter_stock_other);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        buttonAdd10 = (Button) view.findViewById(R.id.button_add_10);
        buttonAdd1 = (Button) view.findViewById(R.id.button_add_1);
        buttonMin1 = (Button) view.findViewById(R.id.button_minus_1);
        buttonMin10 = (Button) view.findViewById(R.id.button_minus_10);

        textName.setEnabled(false);
        textPrice.setEnabled(false);
        textGoldCredit.setEnabled(false);
        textStock.setEnabled(false);
        spinnerCategory.setEnabled(false);
        buttonUpdate.setEnabled(false);
        buttonMin10.setEnabled(false);
        buttonMin1.setEnabled(false);
        buttonAdd1.setEnabled(false);
        buttonAdd10.setEnabled(false);
        buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreen));

        pref = getContext().getApplicationContext().getSharedPreferences("LAGRAMMA", MODE_PRIVATE);
//        if(pref.getString("privilege_id",null).equals("3")){
//            linearLayout.setVisibility(View.GONE);
//        }
        if(pref.getString("privilege_id",null).equals("2")){
            buttonAdd.setVisibility(View.GONE);
        }

        new GetStockTask().execute();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetStockTask().execute();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(itemFilter.equals("")){
                    selectedId = list.get(position).getItemId();
                    textName.setText(list.get(position).getItemName());
                    textPrice.setText(Integer.toString(list.get(position).getItemPrice()));
                    textGoldCredit.setText(Integer.toString(list.get(position).getItemPrice2()));
                    textStock.setText(Integer.toString(list.get(position).getItemStocks()));
                    spinnerCategory.setSelection(list.get(position).getItemCategoryId()-1);
                }else{
                    selectedId = viewList.get(position).getItemId();
                    textName.setText(viewList.get(position).getItemName());
                    textPrice.setText(Integer.toString(viewList.get(position).getItemPrice()));
                    textGoldCredit.setText(Integer.toString(viewList.get(position).getItemPrice2()));
                    textStock.setText(Integer.toString(viewList.get(position).getItemStocks()));
                    spinnerCategory.setSelection(viewList.get(position).getItemCategoryId()-1);
                }




                if(!pref.getString("privilege_id",null).equals("2")){
                    textStock.setEnabled(true);
                    buttonMin10.setEnabled(true);
                    buttonMin1.setEnabled(true);
                    buttonAdd1.setEnabled(true);
                    buttonAdd10.setEnabled(true);
                }

                if(pref.getString("privilege_id",null).equals("4")){
                    textName.setEnabled(true);
                    textPrice.setEnabled(true);
                    textGoldCredit.setEnabled(true);
                    spinnerCategory.setEnabled(true);
                }

                buttonUpdate.setEnabled(true);
                selectedIdx = position;
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNewItemDialog();
            }
        });
        buttonMin10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int stock = Integer.parseInt(textStock.getText().toString());
                stock-=10;
                textStock.setText(String.valueOf(stock));
            }
        });
        buttonMin1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int stock = Integer.parseInt(textStock.getText().toString());
                textStock.setText(String.valueOf(--stock));
            }
        });
        buttonAdd1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int stock = Integer.parseInt(textStock.getText().toString());
                textStock.setText(String.valueOf(++stock));
            }
        });
        buttonAdd10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int stock = Integer.parseInt(textStock.getText().toString());
                stock+=10;
                textStock.setText(String.valueOf(stock));
            }
        });
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = textName.getText().toString();
                int price = Integer.parseInt(textPrice.getText().toString());
                int stock = Integer.parseInt(textStock.getText().toString());
                int goldCredit = Integer.parseInt(textGoldCredit.getText().toString());
                String category = spinnerCategory.getSelectedItem().toString();
                int categoryId = 4;
                for(Option obj : Global.ItemCategoryList){
                    if(obj.getName().contains(category.toLowerCase())){
                        categoryId = obj.getId();
                        break;
                    }
                }
                Item curItem = new Item(selectedId, name, price, goldCredit, stock, category.toLowerCase(), categoryId);
                showConfirmEditDialog(curItem);
            }
        });

        buttonAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFilter = "";
                refreshList();
                buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonNastar.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonLapis.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonPack.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonOther.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonLapis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFilter = "lapis legit";
                refreshList();
                buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonNastar.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonLapis.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonPack.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonOther.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonNastar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFilter = "nastar";
                refreshList();
                buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonNastar.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonLapis.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonPack.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonOther.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonPack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFilter = "packaging";
                refreshList();
                buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonNastar.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonLapis.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonPack.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonOther.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFilter = "others";
                refreshList();
                buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonNastar.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonLapis.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonPack.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonOther.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });

        return view;
    }

    private void showConfirmEditDialog(final Item curItem){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setTitle("Edit Confirmation");
        builder.setMessage("Make sure your change is correct");

        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int updatedIdx = -1;
                        for(Item item : list){
                            if(curItem.getItemId() == item.getItemId()){
                                updatedIdx = list.indexOf(item);
                                selectedIdx = updatedIdx;
                            }
                        }
                        new UpdateStockTask().execute(
                                curItem.getItemName(),
                                Integer.toString(curItem.getItemPrice()),
                                Integer.toString(curItem.getItemPrice2()),
                                Integer.toString(curItem.getItemStocks()),
                                curItem.getItemCategory(),
                                Integer.toString(curItem.getItemCategoryId())
                        );
                        //list.set(updatedIdx, curItem);
                        //refreshList();
                    }
                });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });
        dialog.show();
    }

    private void showNewItemDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater infalter = LayoutInflater.from(getActivity());
        final View view = infalter.inflate(R.layout.dialog_new_item,null);

        final EditText textName = (EditText) view.findViewById(R.id.text_new_item_name);
        final Spinner spinnerCategory = (Spinner) view.findViewById(R.id.spinner_new_item_category);
        final EditText textPrice = (EditText) view.findViewById(R.id.text_new_item_price);
        final EditText textGoldCredit = (EditText) view.findViewById(R.id.text_new_item_price2);
        final EditText textStock = (EditText) view.findViewById(R.id.text_new_item_stock);

        builder.setView(view).setPositiveButton("Add Item", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String category = spinnerCategory.getSelectedItem().toString();
                int categoryId = 4;
                for(Option obj : Global.ItemCategoryList){
                    if(obj.getName().contains(category.toLowerCase())){
                        categoryId = obj.getId();
                        break;
                    }
                }
                new AddStockTask().execute(
                        textName.getText().toString(),
                        textPrice.getText().toString(),
                        textGoldCredit.getText().toString(),
                        textStock.getText().toString(),
                        category.toLowerCase(),
                        Integer.toString(categoryId)
                );
            }
        }).setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });

        dialog.show();
    }

    private void refreshList(){
        viewList = new ArrayList<>();
        if(!itemFilter.equals("")){
            for(Item item : list){
                if(item.getItemCategory().equals(itemFilter)){
                    viewList.add(item);
                }
            }
        }else{
            viewList = list;
        }
        adapter = new StocksAdapter(
                context,
                R.layout.row_item_stocks,
                viewList
        );
        listView.setAdapter(adapter);
    }

    class GetStockTask extends AsyncTask<String, Void, Integer>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Fetching Stock Data..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "GetItem Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            String param = "token=token";
            param = param + "&user="+pref.getString("user_id","");
            WebService webService = null;
            try {
                webService = new WebService("getItem","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            try{
                JSONObject obj = new JSONObject(jsonString);
                String success = obj.getString("status");
                String message = obj.getString("message");
                Log.v("MESSAGE",message);
                if(success.equals("1")){
                    list.clear();
                    JSONArray itemArray = new JSONArray(message);
                    for(int i = 0; i < itemArray.length(); i++){
                        JSONObject itemObject = itemArray.getJSONObject(i);
                        list.add(new Item(
                                itemObject.getInt("item_id"),
                                itemObject.getString("item_name"),
                                itemObject.getInt("item_price"),
                                itemObject.getInt("item_gold_credit"),
                                itemObject.getInt("item_stock"),
                                itemObject.getString("item_category_name"),
                                itemObject.getInt("item_category_id")
                                ));
                    }
                    return 1;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer res) {
            super.onPostExecute(res);
            if (swipeRefreshLayout.isRefreshing())swipeRefreshLayout.setRefreshing(false);
            if(res == 1){

                adapter = new StocksAdapter(
                        context,
                        R.layout.row_item_stocks,
                        list
                );
                listView.setAdapter(adapter);
            }
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

    class AddStockTask extends AsyncTask<String, Void, Integer>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Add Data..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "AddItem Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            String param = "token=token";
            param = param + "&user="+pref.getString("user_id","");
            param = param + "&item_name="+params[0];
            param = param + "&item_price="+params[1];
            param = param + "&item_gold_credit="+params[2];
            param = param + "&item_stock="+params[3];
            param = param + "&item_category_id="+params[5];
            Log.v("PARAM",param);

            WebService webService = null;
            try {
                webService = new WebService("addItem","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            try{
                JSONObject obj = new JSONObject(jsonString);
                String success = obj.getString("status");
                if(success.equals("1")){
                    String id = obj.getString("message");
                    item = new Item(
                            Integer.parseInt(id),
                            params[0],
                            Integer.parseInt(params[1]),
                            Integer.parseInt(params[2]),
                            Integer.parseInt(params[3]),
                            params[4],
                            Integer.parseInt(params[5])
                    );
                    return 1;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer success) {
            super.onPostExecute(success);
            String message = "Add Item Failed";
            if(success == 1){
                list.add(item);
                refreshList();
                progressDialog.hide();
                progressDialog.dismiss();
                message = "Add Item Success";
            }
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    class UpdateStockTask extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Updating Item");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "UpdateItem Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "token=token";
            param = param + "&user="+pref.getString("user_id","");
            param = param + "&item_id="+Integer.toString(selectedId);
            param = param + "&item_name="+params[0];
            param = param + "&item_price="+params[1];
            param = param + "&item_gold_credit="+params[2];
            param = param + "&item_stock="+params[3];
            param = param + "&item_category_id="+params[5];
            Log.v("PARAM",param);
            WebService webService = null;
            try {
                webService = new WebService("updateItem","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            String res = "Failed to update item";
            try{
                JSONObject obj = new JSONObject(jsonString);
                res = obj.getString("message");
                Log.v("RES",res);
                item = new Item(
                        selectedId,
                        params[0],
                        Integer.parseInt(params[1]),
                        Integer.parseInt(params[2]),
                        Integer.parseInt(params[3]),
                        params[4],
                        Integer.parseInt(params[5])
                );
            }catch(Exception e){
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            //Log.v("ITEM NAME",item.getItemName());
            if(!res.contains("Failed")){
                list.set(selectedIdx,item);
                refreshList();
            }
            Toast.makeText(context, res, Toast.LENGTH_SHORT).show();
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }
}
