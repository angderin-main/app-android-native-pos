package com.androidkt.lagramma;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LoginActivity extends AppCompatActivity {

    EditText textUsername, textPassword;
    Button button_login;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    CheckBox checkBox;

    WebService webService;

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("OnResume","Resume");
        Log.e("OnResume", String.valueOf(!pref.getString("username","").equals("")));
        if (!pref.getString("username","").equals("") && !pref.getString("privilege_id","").equals("") && !pref.getString("user_email","").equals("")){
            startActivity(new Intent(LoginActivity.this, MainMenuActivity.class));
            finish();
        }
        if (pref.getBoolean("chbx_remember",false)){
            checkBox.setChecked(pref.getBoolean("chbx_remember",false));
            textUsername.setText(pref.getString("username_remember",""));
            textPassword.setText(pref.getString("password_remember",""));
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("OnRestart","Restart");


    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        textUsername = (EditText) findViewById(R.id.text_login_username);
        textPassword = (EditText) findViewById(R.id.text_login_password);
        button_login = (Button) findViewById(R.id.button_login);
        checkBox = (CheckBox) findViewById(R.id.chk_box_remember_me);

        pref = getApplicationContext().getSharedPreferences("LAGRAMMA", MODE_PRIVATE);
        editor = pref.edit();

        if (pref.getBoolean("chbx_remember",false)){
            checkBox.setChecked(pref.getBoolean("chbx_remember",false));
            textUsername.setText(pref.getString("username_remember",""));
            textUsername.setText(pref.getString("password_remember",""));
        }

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = textUsername.getText().toString();
                String password = textPassword.getText().toString();
                //String param = "user_name=" + username + "&user_password=" + password;
                //webService = new WebService("login","POST",param);
                //webService = new WebService("getPrivilege","POST","");
                //Log.d("Response", webService.responseBody);
                //startActivity(new Intent(LoginActivity.this, MainMenuActivity.class));
                if(hasText(textUsername) && hasText(textPassword)){
                    new LoginTask().execute(username,password);
                    //Toast.makeText(LoginActivity.this, "xyz", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public static boolean hasText(EditText editText) {
        String text = editText.getText().toString().trim();
        editText.setError(null);
        if (text.length() == 0) {
            editText.setError("Silahkan input data ini terlebih dahulu");
            editText.requestFocus();
            return false;
        }
        return true;
    }

    private class LoginTask extends AsyncTask<String, Void, String[]> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Loggin in...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(LoginActivity.this, "Login Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String[] doInBackground(String... params) {
            String username = params[0];
            String pass = params[1];
            WebService webService = null;
            try {
                webService = new WebService("login", "POST", "user_email="+username+"&user_password="+pass);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;

            String[] result = new String[2];
            try {
                JSONObject obj = new JSONObject(jsonString);
                result[0] = obj.getString("status");
                result[1] = obj.getString("message");
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);

            if(result[0].equals("0")){
                Toast.makeText(LoginActivity.this, result[1], Toast.LENGTH_SHORT).show();
            }else{
                String username = null, privilegeId = null, email = null, userid = null;
                try {
                    JSONObject obj = new JSONObject(result[1]);
                    username = obj.getString("user_name");
                    privilegeId = obj.getString("fk_privilege_id");
                    email = obj.getString("user_email");
                    userid = obj.getString("user_id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                editor.putString("username",username);
                editor.putString("privilege_id",privilegeId);
                editor.putString("user_email",email);
                editor.putString("user_id",userid);

                if (checkBox.isChecked()){
                    Log.e("RememberMe", String.valueOf(checkBox.isChecked()));
                    editor.putString("username_remember",textUsername.getText().toString());
                    editor.putString("password_remember",textPassword.getText().toString());
                    editor.putBoolean("chbx_remember",true);
                }else {
                    editor.remove("username_remember");
                    editor.remove("password_remember");
                    editor.remove("chbx_remember");
                }
                editor.apply();

                //Toast.makeText(LoginActivity.this, username, Toast.LENGTH_SHORT).show();
                progressDialog.hide();
                progressDialog.dismiss();
                new PrivilegeTask().execute();
            }
        }
    }

    public class PrivilegeTask extends AsyncTask<String, Void, Boolean> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Get Data..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(LoginActivity.this, "GetPrivilege Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            Boolean res = true;
            WebService webService = null;
            try {
                webService = new WebService("getPrivilege", "POST", "");
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            List<Option> privilegeList = new ArrayList<>();
            try {
                JSONObject obj = new JSONObject(jsonString);
                String success = obj.getString("status");
                if(success.equals("1")) {
                    String privilege = obj.getString("message");
                    JSONArray privilegeArray = new JSONArray(privilege);
                    for (int i = 0; i < privilegeArray.length(); i++) {
                        JSONObject privilegeObject = privilegeArray.getJSONObject(i);
                        int privilege_id = privilegeObject.getInt("privilege_id");
                        String privilege_name = privilegeObject.getString("privilege_name");
                        privilegeList.add(new Option(privilege_id,privilege_name));
                    }
                    Global.PrivilegeList = privilegeList;
                }else{
                    res = false;
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            WebService webService2 = null;
            try {
                webService2 = new WebService("getItemCategory", "POST", "token=token");
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString2 = webService2.responseBody;
            List<Option> itemCategoryList = new ArrayList<>();
            try{
                JSONObject obj = new JSONObject(jsonString2);
                String success = obj.getString("status");
                if(success.equals("1")){
                    String itemCategory = obj.getString("message");
                    JSONArray itemCatArray = new JSONArray(itemCategory);
                    for(int i = 0; i < itemCatArray.length(); i++){
                        JSONObject itemCatObject = itemCatArray.getJSONObject(i);
                        int item_category_id = itemCatObject.getInt("item_category_id");
                        String item_category_name = itemCatObject.getString("item_category_name");
                        itemCategoryList.add(new Option(item_category_id, item_category_name));
                    }
                    Global.ItemCategoryList = itemCategoryList;
                }else{
                    res = false;
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            WebService cashTypeService = null;
            try {
                cashTypeService = new WebService("getCashType","POST","token=token");
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString3 = cashTypeService.responseBody;
            List<Option> cashTypeList = new ArrayList<>();
            try{
                JSONObject obj = new JSONObject(jsonString3);
                String success = obj.getString("status");
                if(success.equals("1")){
                    String message = obj.getString("message");
                    JSONArray jsonArray = new JSONArray(message);
                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int id = jsonObject.getInt("cash_type_id");
                        String name = jsonObject.getString("cash_type_name");
                        Log.v("CASH TYPE",name);
                        cashTypeList.add(new Option(id,name));
                    }
                    Global.CashTypeList = cashTypeList;
                }else{
                    res = false;
                }
            }catch(Exception e){
                e.printStackTrace();
            }

            WebService orderStatusService = null;
            try {
                orderStatusService = new WebService("getOrderStatus","POST","token=token");
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString4 = orderStatusService.responseBody;
            List<Option> orderStatusList = new ArrayList<>();
            try{
                JSONObject obj = new JSONObject(jsonString4);
                String success = obj.getString("status");
                if(success.equals("1")){
                    String message = obj.getString("message");
                    JSONArray jsonArray = new JSONArray(message);
                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int id = jsonObject.getInt("order_status_id");
                        String name = jsonObject.getString("order_status_name");
                        Log.v("ORDER STATUS",name);
                        orderStatusList.add(new Option(id,name));
                    }
                    Global.OrderStatusList = orderStatusList;
                }else{
                    res = false;
                }
            }catch(Exception e){
                e.printStackTrace();
            }

            WebService paymentStatusService = null;
            try {
                paymentStatusService = new WebService("getPaymentStatus","POST","token=token");
            } catch (SocketTimeoutException e) {
                e.printStackTrace();

            }
            String jsonString5 = paymentStatusService.responseBody;
            List<Option> paymentStatusList = new ArrayList<>();
            try{
                JSONObject obj = new JSONObject(jsonString5);
                String success = obj.getString("status");
                if(success.equals("1")){
                    String message = obj.getString("message");
                    JSONArray jsonArray = new JSONArray(message);
                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int id = jsonObject.getInt("payment_status_id");
                        String name = jsonObject.getString("payment_status_name");
                        Log.v("PAYMENT STATUS",name);
                        paymentStatusList.add(new Option(id,name));
                    }
                    Global.PaymentStatusList = paymentStatusList;
                }else{
                    res = false;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            progressDialog.hide();
            progressDialog.dismiss();
            if(result){
                startActivity(new Intent(LoginActivity.this, MainMenuActivity.class));
                finish();
            }else{
                Toast.makeText(LoginActivity.this, "Please Try Login Again, Cannot Get Data", Toast.LENGTH_LONG).show();
            }
        }
    }
}
