package com.androidkt.lagramma;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Derin Anggara on 1/14/2018.
 */

public class UserData {
    private ArrayList<User> userList, list;
    private Context context;
    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;

    public UserData(Context context, ListView listView, ArrayList<User> list){
        userList = new ArrayList<>();
        this.context = context;
        this.listView = listView;
        this.list = list;
    }

    public void registerUser(ArrayList<User> list, User newUser){
        new RegisterTask().execute(newUser);
    }

    public void populateData(SwipeRefreshLayout swipeRefreshLayout){
        this.swipeRefreshLayout = swipeRefreshLayout;
        new UserTask().execute();
    }

    public void populateData(){
        new UserTask().execute();
    }

    public void updateUser(User updatedUser){
        new UpdateTask().execute(updatedUser);
    }

    private class UserTask extends AsyncTask<Void, Void, Integer> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Fetch User Data");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "GetUser Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            WebService webService = null;
            try {
                webService = new WebService("getUser", "POST", "token=asal_aja");
            } catch (SocketTimeoutException e) {
//                e.printStackTrace();
                Log.e("TIMEOUT","GetUser");
                cancel(true);
            }
            String jsonString = webService.responseBody;
            Log.e("PopulateData","DoInBackground");
            String[] result = new String[2];
            try {
                JSONObject obj = new JSONObject(jsonString);
                result[0] = obj.getString("status");
                result[1] = obj.getString("message");
                if (result[0].equals("1")){
                    JSONArray users = new JSONArray(result[1]);
                    int i = 0;
                    while (i < users.length()){
                        JSONObject user = users.getJSONObject(i);

                        User temp = new User(
                                user.getInt("user_id"),
                                user.getString("user_name"),
                                user.getString("user_email"),
                                user.getInt("fk_privilege_id"),
                                user.getString("privilege_name")
                        );
                        userList.add(temp);

                        i++;
                    }

                    return 1;
                }
            }catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            progressDialog.hide();
            progressDialog.dismiss();
            if(swipeRefreshLayout!=null)swipeRefreshLayout.setRefreshing(false);
            if (result == 1){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                listView.setAdapter(new UserAdapter(
                        context,
                        R.layout.row_item_user,
                        userList
                ));
                listView.startAnimation(AnimationUtils.loadAnimation(context,android.R.anim.slide_in_left));
                list.clear();
                list.addAll(userList);
            }
        }
    }

    private class RegisterTask extends AsyncTask<User, Void, Integer> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Registering User Data");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "Register Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Integer doInBackground(User... params) {
            User user = params[0];
            WebService webService = null;
            try {
                webService = new WebService(
                        "register",
                        "POST",
                        "token=asal_aja"+
                        "&user_name="+user.get_user_name()+
                        "&user_email="+user.get_user_email()+
                        "&user_password="+user.get_user_password()+
                        "&fk_privilege_id="+user.get_fk_privilege_id());
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;

            String[] result = new String[2];
            try {
                JSONObject obj = new JSONObject(jsonString);
                result[0] = obj.getString("status");
                result[1] = obj.getString("message");
                if (result[0].equals("1")){
//                    user.set_user_id(Integer.parseInt(result[1]));
//                    userList.add(user);
                    userList.add(new User(
                            Integer.parseInt(result[1]),
                            user.get_user_name(),
                            user.get_user_email(),
                            user.get_fk_privilege_id(),
                            user.get_privilege_name()
                    ));
                    return 1;
                }
            }catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            progressDialog.hide();
            if (result == 1){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                listView.setAdapter(new UserAdapter(
                        context,
                        R.layout.row_item_user,
                        userList
                ));
                listView.startAnimation(AnimationUtils.loadAnimation(context,android.R.anim.slide_in_left));
                list.clear();
                list.addAll(userList);
            }
        }
    }

    private class UpdateTask extends AsyncTask<User, Void, Integer>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Update User Data");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(User... params) {
            User user = params[0];
            WebService webService = null;
            try {
                Log.v("PARAM","&user_id="+user.get_user_id()+
                        "&user_name="+user.get_user_name()+
                        "&user_email="+user.get_user_email()+
                        "&user_password="+user.get_user_password()+
                        "&fk_privilege_id="+user.get_fk_privilege_id());
                webService = new WebService(
                        "updateUser",
                        "POST",
                        "token=asal_aja"+
                                "&user_id="+user.get_user_id()+
                                "&user_name="+user.get_user_name()+
                                "&user_email="+user.get_user_email()+
                                "&user_password="+user.get_user_password()+
                                "&fk_privilege_id="+user.get_fk_privilege_id());
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
            }
            String jsonString = webService.responseBody;

            String[] result = new String[2];
            try {
                JSONObject obj = new JSONObject(jsonString);
                result[0] = obj.getString("status");
                result[1] = obj.getString("message");
                if (result[0].equals("1")){
//                    user.set_user_id(Integer.parseInt(result[1]));
//                    userList.add(user);
//                    userList.add(new User(
//                            Integer.parseInt(result[1]),
//                            user.get_user_name(),
//                            user.get_user_email(),
//                            user.get_fk_privilege_id(),
//                            user.get_privilege_name()
//                    ));
                    for( User x : userList){
                        if(x.get_user_id() == user.get_user_id()){
                            userList.set(userList.indexOf(x),user);
                            break;
                        }
                    }
                    return 1;
                }
            }catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }



            return 1;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            progressDialog.hide();
            if (integer == 1){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                listView.setAdapter(new UserAdapter(
                        context,
                        R.layout.row_item_user,
                        userList
                ));
                listView.startAnimation(AnimationUtils.loadAnimation(context,android.R.anim.slide_in_left));
                list.clear();
                list.addAll(userList);
            }
        }
    }
}
