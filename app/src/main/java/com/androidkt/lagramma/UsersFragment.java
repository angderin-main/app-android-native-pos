package com.androidkt.lagramma;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class UsersFragment extends Fragment {

    User selectedUser;

    ListView listUsers;
    Button buttonAddUser, buttonEditUser;
    EditText textUpdateName, textUpdateEmail, textUpdatePassword;
    Spinner spinnerUpdatePriority;
    SwipeRefreshLayout swipeRefreshLayout;

    ArrayList<User> list = new ArrayList<>();
    WebService webService;
    UserAdapter adapter;
    UserData userData;

    public UsersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_users, container, false);
        listUsers = (ListView) view.findViewById(R.id.list_users);
        userData = new UserData(getContext(),listUsers, list);
        userData.populateData();

        buttonAddUser = (Button) view.findViewById(R.id.button_add_user);
        buttonEditUser = (Button) view.findViewById(R.id.button_edit_user);
        textUpdateName = (EditText) view.findViewById(R.id.text_update_name);
        textUpdateEmail = (EditText) view.findViewById(R.id.text_update_email);
        textUpdatePassword = (EditText) view.findViewById(R.id.text_update_password);
        spinnerUpdatePriority = (Spinner) view.findViewById(R.id.spinner_update_priority);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e("SWIPE","DRAGED");
                userData.populateData(swipeRefreshLayout);
            }
        });

        listUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedUser = list.get(position);
                textUpdateName.setText(selectedUser.get_user_name());
                textUpdateEmail.setText(selectedUser.get_user_email());
                textUpdatePassword.setText("");
                Log.e("UpdateUser",Integer.toString(selectedUser.get_fk_privilege_id()));
                switch (selectedUser.get_fk_privilege_id()){
                    case 2 : spinnerUpdatePriority.setSelection(0);break;
                    case 3 : spinnerUpdatePriority.setSelection(1);break;
                    case 4 : spinnerUpdatePriority.setSelection(2);break;
                    default: spinnerUpdatePriority.setSelection(0);break;
                }
            }
        });
        buttonAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddUser();
            }
        });
        buttonEditUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("UpdateButton",spinnerUpdatePriority.getSelectedItem().toString());
                int privilege = 0;
                for(Option o : Global.PrivilegeList){
                    if(o.getName().equals(spinnerUpdatePriority.getSelectedItem().toString().toLowerCase())){
                        privilege = o.getId();
                    }
                }
                User tempUser = new User(
                        selectedUser.get_user_id(),
                        textUpdateName.getText().toString(),
                        textUpdateEmail.getText().toString(),
                        textUpdatePassword.getText().toString(),
                        privilege,
                        spinnerUpdatePriority.getSelectedItem().toString().toLowerCase()
                );
                userData.updateUser(tempUser);
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    public void showDialogAddUser(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater infalter = LayoutInflater.from(getActivity());
        final View view = infalter.inflate(R.layout.dialog_new_user,null);

        final EditText textUsername = (EditText) view.findViewById(R.id.text_new_user_name);
        final EditText textPassword = (EditText) view.findViewById(R.id.text_new_user_password);
        final EditText textEmail = (EditText) view.findViewById(R.id.text_new_user_email);
        final Spinner spinnerPrior = (Spinner) view.findViewById(R.id.spinner_new_user_priority);

        builder.setView(view)
                .setPositiveButton("Add User",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String name = textUsername.getText().toString();
                                String email = textEmail.getText().toString();
                                String password = textPassword.getText().toString();
                                String prior = spinnerPrior.getSelectedItem().toString();
                                int id = -1;
                                for(Option obj : Global.PrivilegeList){
                                    if(obj.getName().contains(prior.toLowerCase())){
                                        id = obj.getId();
                                        break;
                                    }
                                }
                                userData.registerUser(
                                        list,
                                        new User(
                                                name,
                                                email,
                                                password,
                                                id,
                                                prior
                                        )
                                );
//                                new RegisterTask().execute(name,email,password,id);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });

        dialog.show();
    }
}
