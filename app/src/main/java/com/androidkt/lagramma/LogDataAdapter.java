package com.androidkt.lagramma;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Derin Anggara on 4/6/2018.
 */

public class LogDataAdapter extends ArrayAdapter<LogData> {

    private TextView logDate;

    ArrayList<LogData> data;

    public LogDataAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<LogData> objects) {
        super(context, resource, objects);
        this.data = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;

        if(view == null){
            LayoutInflater layoutInflater;
            layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(R.layout.row_item_log_data,null);
            logDate = (TextView) view.findViewById(R.id.log_date);
        }

        LogData log = data.get(position);

        if(log != null){
            logDate.setText(log.getDate());
        }

        return view;
    }

    @Override
    public int getViewTypeCount() {
        if(getCount() < 1){
            return 1;
        }
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
