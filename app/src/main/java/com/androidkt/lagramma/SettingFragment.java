package com.androidkt.lagramma;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    Button btnConnect;
    EditText printerName;
    TextView connectionStatus;
    private static String PRINTER_STATUS = "Printer Status : ";
    Context context;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_setting, container, false);
        context = getContext();

        connectionStatus = (TextView) view.findViewById(R.id.txtPrinterStatus);
        printerName = (EditText) view.findViewById(R.id.txtPrinterName);
        btnConnect = (Button) view.findViewById(R.id.btnConnect);

        if(Global.printerStatus){
            connectionStatus.setText(PRINTER_STATUS+"Connected");
            btnConnect.setText("Disconnect");
        }
        else{
            connectionStatus.setText(PRINTER_STATUS+"Disconnected");
            btnConnect.setText("Connect");
        }

        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Global.printerStatus){
                    new ConnectTask().execute();
                    connectionStatus.setText(PRINTER_STATUS+"Connected");
                    btnConnect.setText("Disconnect");
                    Global.printerStatus = true;
                }
                else{
                    new DisconnectTask().execute();
                    connectionStatus.setText(PRINTER_STATUS+"Disconnected");
                    btnConnect.setText("Connect");
                    Global.printerStatus = false;
                }
            }
        });

        return view;
    }

    private class ConnectTask extends AsyncTask<String, Void, String[]> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Connecting...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "Connection Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String[] doInBackground(String... params) {
            String[] result = new String[2];
            Global.printModel = new ModelPrint();
            Global.printModel.connectPrinter(printerName.getText().toString());
            return result;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

    private class DisconnectTask extends AsyncTask<String, Void, String[]> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Disconnecting...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "Connection Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String[] doInBackground(String... params) {
            String[] result = new String[2];
            Global.printModel.disconnectPrinter();
            return result;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

}
