package com.androidkt.lagramma;

import java.util.Date;

/**
 * Created by Tommy on 23/12/2017.
 */

public class Orders {
    private int orderId;
    private String orderName;
    private String orderstatus;
    private Date orderDatetime;
    private int orderTotal;

    public int getOrderId() {
        return orderId;
    }

    public String getOrderName() {
        return orderName;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public Date getOrderDatetime() {
        return orderDatetime;
    }

    public int getOrderTotal() {
        return orderTotal;
    }

    public Orders(int orderId, String orderName, String orderstatus, Date orderDatetime, int orderTotal) {
        this.orderId = orderId;
        this.orderName = orderName;
        this.orderstatus = orderstatus;
        this.orderDatetime = orderDatetime;
        this.orderTotal = orderTotal;
    }
}
