package com.androidkt.lagramma;

/**
 * Created by aldo_ on 10/03/2018.
 */

public class ModelPembayaran {
    private String _nama;
    private int _jumlah;
    private int _hargasatuan;
    public ModelPembayaran(String nama, int jumlah, int hargasatuan){
        this._nama = nama;
        this._jumlah = jumlah;
        this._hargasatuan = hargasatuan/jumlah;
    }
    public String get_nama() {
        return _nama;
    }
    public int get_jumlah() {
        return _jumlah;
    }
    public int get_hargasatuan() {
        return _hargasatuan;
    }
}
