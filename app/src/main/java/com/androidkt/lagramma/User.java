package com.androidkt.lagramma;

/**
 * Created by Tommy on 23/12/2017.
 */

public class User {

    private int _user_id;
    private String _user_name;
    private String _user_email;
    private String _user_password;
    private int _fk_privilege_id;
    private String _privilege_name;

    public int get_user_id() {
        return _user_id;
    }

    public void set_user_id(int _user_id) {
        this._user_id = _user_id;
    }

    public String get_user_name() {
        return _user_name;
    }

    public void set_user_name(String _user_name) {
        this._user_name = _user_name;
    }

    public String get_user_email() {
        return _user_email;
    }

    public void set_user_email(String _user_email) {
        this._user_email = _user_email;
    }

    public String get_user_password() {
        return _user_password;
    }

    public void set_user_password(String _user_password) {
        this._user_password = _user_password;
    }

    public int get_fk_privilege_id() {
        return _fk_privilege_id;
    }

    public void set_fk_privilege_id(int _fk_privilege_id) {
        this._fk_privilege_id = _fk_privilege_id;
    }

    public String get_privilege_name() {
        return _privilege_name;
    }

    public void set_privilege_name(String _privilege_name) {
        this._privilege_name = _privilege_name;
    }

    public User(int user_id ,String user_name, String user_email, int fk_privilege_id, String privilege_name) {
        _user_id = user_id;
        _user_name = user_name;
        _user_email = user_email;
        _fk_privilege_id = fk_privilege_id;
        _privilege_name = privilege_name;
    }

    public User(String _user_name, String _user_email, String _user_password, int _fk_privilege_id, String _privilege_name) {
        this._user_name = _user_name;
        this._user_email = _user_email;
        this._user_password = _user_password;
        this._fk_privilege_id = _fk_privilege_id;
        this._privilege_name = _privilege_name;
    }

    public User(int _user_id, String _user_name, String _user_email, String _user_password, int _fk_privilege_id, String _privilege_name) {
        this._user_id = _user_id;
        this._user_name = _user_name;
        this._user_email = _user_email;
        this._user_password = _user_password;
        this._fk_privilege_id = _fk_privilege_id;
        this._privilege_name = _privilege_name;
    }
}
