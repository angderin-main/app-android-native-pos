package com.androidkt.lagramma;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tommy on 02/01/2018.
 */

public class StocksAdapter extends ArrayAdapter<Item>{

    TextView textName, textStock, textPrice, textPrice2;

    private ArrayList<Item> data;


    public StocksAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<Item> objects) {
        super(context, resource, objects);
        this.data = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;


        if(view == null){
            LayoutInflater layoutInflater;
            layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(R.layout.row_item_stocks,null);
            textName = (TextView) view.findViewById(R.id.text_stock_name);
            textPrice = (TextView) view.findViewById(R.id.text_stock_price);
            textStock = (TextView) view.findViewById(R.id.text_stock_stock);
            textPrice2 = (TextView) view.findViewById(R.id.text_stock_price2);
        }

        Item stock = data.get(position);

        if(stock != null){
            textName.setText(stock.getItemName());
            textStock.setText(Integer.toString(stock.getItemStocks()));
            textPrice.setText(Integer.toString(stock.getItemPrice()));
            textPrice2.setText(Integer.toString(stock.getItemPrice2()));
        }

        if(position%2 == 0){
            view.setBackgroundColor(Color.parseColor("#E8F5E9"));
        }else{
            view.setBackgroundColor(Color.parseColor("#D8EED9"));
        }

        return view;
    }

    @Override
    public int getViewTypeCount() {
        if(getCount() < 1){
            return 1;
        }
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
