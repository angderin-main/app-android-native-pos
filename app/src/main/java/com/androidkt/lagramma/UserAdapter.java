package com.androidkt.lagramma;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tommy on 23/12/2017.
 */

public class UserAdapter extends ArrayAdapter<User> {

    private ArrayList<User> data;

    public UserAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<User> data) {
        super(context, resource, data);
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if(view == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.row_item_user, null);

            TextView textUserName = (TextView) view.findViewById(R.id.text_user_name);
            TextView textUserEmail = (TextView) view.findViewById(R.id.text_user_email);
            TextView textUserStatus = (TextView) view.findViewById(R.id.text_user_status);

            User user = data.get(position);
            if(user != null){
                textUserName.setText(user.get_user_name());
                textUserEmail.setText(user.get_user_email());
                textUserStatus.setText(user.get_privilege_name());
//                textUserName.setText(user.getName());
//                textUserStatus.setText(user.getStatus());
            }
        }
        if(position%2 == 0){
            view.setBackgroundColor(Color.parseColor("#E8F5E9"));
        }else{
            view.setBackgroundColor(Color.parseColor("#D8EED9"));
        }
        return view;
    }

    @Override
    public int getViewTypeCount() {
        if(getCount() < 1){
            return 1;
        }
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
