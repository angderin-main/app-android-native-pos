package com.androidkt.lagramma;

import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.concurrent.TimeoutException;

/**
 * Created by Tommy on 02/01/2018.
 */

public class WebService {
    private String baseUrl = "http://lagramma.com/API/";
    public String responseBody;

    public WebService(String urlString, String method, String params) throws SocketTimeoutException {
        try {
            URL url = new URL(baseUrl+urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(2000);
            connection.connect();
            if (method.equals("POST")) {
                connection.getOutputStream().write(params.getBytes());
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder lines = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                lines.append(line);
            }
            responseBody = lines.toString();
        } catch (SocketTimeoutException e){
            Log.e("TIMEOUT","Webservice");
            throw new SocketTimeoutException("TIMEOUT");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
