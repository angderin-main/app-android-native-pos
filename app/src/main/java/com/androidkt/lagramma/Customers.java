package com.androidkt.lagramma;

import android.support.annotation.NonNull;
import android.text.Editable;

/**
 * Created by Tommy on 20/12/2017.
 */

public class Customers {

    private final int id;
    private String name;
    private String phone;
    private String address;
    private boolean priority;
    private int priorityNumber;
    private int credit;

    private boolean sortPriority;

    public Customers(int id, String name, String phone, String address, boolean priority, int priorityNumber, int credit) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.priority = priority;
        this.priorityNumber = priorityNumber;
        this.credit = credit;
    }

    public Customers(String name, String phone, String address, boolean priority) {
        this.id = 0;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.priority = priority;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public boolean isPriority() {
        return priority;
    }

    public int getPriorityNumber() {
        return priorityNumber;
    }

    public int getCredit() {
        return credit;
    }

}
