package com.androidkt.lagramma;

/**
 * Created by Tommy on 27/01/2018.
 */

public class Option {
    private int id;
    private String name;

    public Option(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
