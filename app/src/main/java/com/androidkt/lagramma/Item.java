package com.androidkt.lagramma;

/**
 * Created by Derin Anggara on 12/21/2017.
 */

public class Item {


    private int itemId;
    private String itemName;
    private int itemPrice;
    private int itemPrice2;
    private int itemStocks;
    private String itemCategory;
    private int itemCategoryId;


    public Item(int itemId, String itemName, int itemPrice, int itemPrice2, int itemStocks, String itemCategory, int itemCategoryId) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemPrice2 = itemPrice2;
        this.itemStocks = itemStocks;
        this.itemCategory = itemCategory;
        this.itemCategoryId = itemCategoryId;
    }

//    public Item(String itemName, int itemPrice, int itemStocks) {
//        this.itemName = itemName;
//        this.itemPrice = itemPrice;
//        this.itemStocks = itemStocks;
//    }

    public Item(int itemId, String itemName, int itemPrice, int itemStocks) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemStocks = itemStocks;
    }

    public String getItemName() {return itemName; }

    public int getItemPrice() {
        return itemPrice;
    }

    public int getItemStocks() {
        return itemStocks;
    }

    public int getItemPrice2() { return itemPrice2; }

    public int getItemId() { return itemId; }

    public String getItemCategory() { return itemCategory; }

    public int getItemCategoryId() { return itemCategoryId; }
}
