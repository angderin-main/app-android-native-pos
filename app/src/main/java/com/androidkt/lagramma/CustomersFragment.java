package com.androidkt.lagramma;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.ObservableArrayList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class CustomersFragment extends Fragment {

    CustomersAdapter adapter;
    ListView listViewItems;
    Spinner spinnerPrior;
    EditText textName, textPhone, textAddress, textPriorNumber, customerSearch, textMinCredit;
    TextView textCredit, textRemainCredit;
    Button buttonAdd, buttonUpdate, buttonSortAll, buttonSortGold;
    LinearLayout linearLayout;
    SwipeRefreshLayout swipeRefreshLayout;

    SharedPreferences pref;

    int selectedIdx;
    int selectedId;
    ArrayList<Customers> list = new ArrayList<>();
    ArrayList<Customers> viewList;
    protected Customers cust;
    boolean currFilter = false;
    Context context;

    public CustomersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_customers, container, false);
        context = getContext();
        //Initialize all component in XML
        textName = (EditText) view.findViewById(R.id.text_update_name);
        textPhone = (EditText) view.findViewById(R.id.text_update_phone);
        textAddress = (EditText) view.findViewById(R.id.text_update_address);
        customerSearch = (EditText) view.findViewById(R.id.text_customer_search);
        spinnerPrior = (Spinner) view.findViewById(R.id.spinner_priority);
        //textPriorNumber = (EditText) view.findViewById(R.id.text_update_priority_number);
        textCredit = (TextView) view.findViewById(R.id.text_detail_credit);
        textMinCredit = (EditText) view.findViewById(R.id.text_min_credit);
        textRemainCredit = (TextView) view.findViewById(R.id.text_remain_credit);
        buttonUpdate = (Button) view.findViewById(R.id.button_edit_customer);
        buttonAdd = (Button) view.findViewById(R.id.button_add_customer);
        buttonSortAll = (Button) view.findViewById(R.id.btn_sort_all);
        buttonSortGold = (Button) view.findViewById(R.id.btn_sort_gold);
        linearLayout = (LinearLayout) view.findViewById(R.id.linear_customer_detail);
        listViewItems = (ListView) view.findViewById(R.id.list_view_item);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);

        pref = getActivity().getSharedPreferences("LAGRAMMA", MODE_PRIVATE);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetCustomerTask().execute();
            }
        });

        final Animation anim = AnimationUtils.loadAnimation(
                getContext(), android.R.anim.slide_in_left
        );
        anim.setDuration(500);

        if(!pref.getString("privilege_id",null).equals("4")){
            linearLayout.setVisibility(View.GONE);
        }

        buttonSortAll.setBackgroundColor(getResources().getColor(R.color.colorGreen));

        //Disabled the component
        textName.setEnabled(false);
        textPhone.setEnabled(false);
        textAddress.setEnabled(false);
        spinnerPrior.setEnabled(false);
        //textPriorNumber.setEnabled(false);
        buttonUpdate.setEnabled(false);
        textMinCredit.setEnabled(false);

        new GetCustomerTask().execute();

        //Listview clicked
        listViewItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedId = viewList.get(position).getId();

                textName.setText(viewList.get(position).getName().toString());
                textPhone.setText(viewList.get(position).getPhone().toString());
                textAddress.setText(viewList.get(position).getAddress().toString());
                if(viewList.get(position).isPriority()){
                    spinnerPrior.setSelection(1);
                    textMinCredit.setEnabled(true);
                    //textPriorNumber.setText(Integer.toString(viewList.get(position).getPriorityNumber()));
                }else{
                    spinnerPrior.setSelection(0);
                    textMinCredit.setEnabled(false);
                    //textPriorNumber.setText("0");
                }
                String max = String.valueOf(viewList.get(position).getCredit());
                textCredit.setText(max);
                textMinCredit.setFilters(new InputFilter[]{new InputFilterMinMax("0",max)});
                textMinCredit.setText("0");
                textRemainCredit.setText(max);
                textName.setEnabled(true);
                textPhone.setEnabled(true);
                textAddress.setEnabled(true);
                spinnerPrior.setEnabled(true);
                //textPriorNumber.setEnabled(true);
                buttonUpdate.setEnabled(true);
                selectedIdx = position;
            }
        });

        textMinCredit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int max = viewList.get(selectedIdx).getCredit();
                if(textMinCredit.length() != 0){
                    int cur = Integer.parseInt(s.toString());
                    textRemainCredit.setText(String.valueOf(max - cur));
                }else{
                    textRemainCredit.setText(String.valueOf(max));
                }
            }
        });
        //Button Sort ALl
        buttonSortAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<Customers> temp = new ArrayList<>(list);
                viewList = new ArrayList<>();
                if (!customerSearch.getText().equals("")){
                    for (Customers customer : temp){
                        if (customer.getName().contains(customerSearch.getText())){
                            viewList.add(customer);
                        }
                    }
                }
                adapter = new CustomersAdapter(
                        getContext(),
                        R.layout.row_item_customers,
                        viewList
                );
                listViewItems.setAdapter(adapter);
                buttonSortAll.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonSortGold.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                anim.reset();
                listViewItems.startAnimation(anim);
                currFilter = false;
            }
        });

        buttonSortGold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(), list.get(0).getName(), Toast.LENGTH_SHORT).show();
                //Sort by gold
//                CharSequence cs = "gold";
                ArrayList<Customers> temp = new ArrayList<>();
                for (Customers customer : list){
                    if(customer.isPriority())
                        temp.add(customer);
                }
                viewList = new ArrayList<Customers>();
                if (!customerSearch.getText().equals("")){
                    for (Customers customer : temp){
                        if (customer.getName().contains(customerSearch.getText())){
                            viewList.add(customer);
                        }
                    }
                }
//                adapter.getFilter().filter(cs);
                adapter = new CustomersAdapter(
                        getContext(),
                        R.layout.row_item_customers,
                        viewList
                );
                listViewItems.setAdapter(adapter);
                buttonSortGold.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonSortAll.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                anim.reset();
                listViewItems.startAnimation(anim);
                currFilter = true;
            }
        });

        //button update clicked
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = selectedId;
                String name = textName.getText().toString();
                String phone = textPhone.getText().toString();
                String address = textAddress.getText().toString();
                boolean prior = false;
                int priorNumber = 0;
                int credit = 0;
                if(spinnerPrior.getSelectedItemPosition() == 1){
                    prior = true;
                    //priorNumber = Integer.parseInt(textPriorNumber.getText().toString());
                }
                if(list.get(selectedIdx).getCredit() > 0){
                    credit = Integer.parseInt(textCredit.getText().toString());
                }

                if(hasText(textName) && hasText(textPhone) && hasText(textAddress)) //&& hasText(textPriorNumber))
                {
                    Customers upCustomer = new Customers(id, name, phone, address, prior, priorNumber, credit);
                    //Toast.makeText(getContext(), list.get(selectedIdx).getName(), Toast.LENGTH_SHORT).show();
                    showConfirmEditDialog(upCustomer);
                }
            }
        });

        customerSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<Customers> temp = new ArrayList<Customers>();
                viewList = new ArrayList<>();

                if (currFilter){
                    for (Customers customer : list){
                        if(customer.isPriority())
                            temp.add(customer);
                    }
                }else{
                    temp.addAll(list);
                }

                for (Customers customer : temp){
                    if (customer.getName().contains(s)){
                        viewList.add(customer);
                    }
                }
                adapter = new CustomersAdapter(
                        getContext(),
                        R.layout.row_item_customers,
                        viewList
                );

                listViewItems.setAdapter(adapter);
//                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        listViewItems.setAnimation(anim);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNewCustomerDialog();
            }
        });

        return view;
    }

    public void showConfirmEditDialog(final Customers cust){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setTitle("Edit Confirmation");
        builder.setMessage("Make sure your change is correct");
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        int updatedIndex = -1;
                        for (Customers customer : list){
//                            Log.e("Customer Update", Integer.toString(cust.getId()));
//                            Log.e("Customer Update 2", Integer.toString(customer.getId()));
                            if(customer.getId() == cust.getId()){
                                updatedIndex = list.indexOf(customer);
                            }
                        }

                        // API Update
                        new EditCustomerTask().execute(
                                Integer.toString(cust.getId()),
                                cust.getName(),
                                cust.getPhone(),
                                cust.getAddress(),
                                Boolean.toString(cust.isPriority()),
                                textRemainCredit.getText().toString()
                        );

                        list.set(updatedIndex,cust);
                        if (currFilter){
                            viewList = new ArrayList<>();
                            for (Customers customer : list){
                                if(customer.isPriority())
                                    viewList.add(customer);
                            }
                        }
                        else
                            viewList = new ArrayList<>(list);

                        adapter = new CustomersAdapter(
                                getContext(),
                                R.layout.row_item_customers,
                                viewList
                        );
                        listViewItems.setAdapter(adapter);


//                        Animation anim = AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left);
//                        listViewItems.getChildAt(updatedIndex).startAnimation(anim);
//                        adapter.notifyDataSetChanged();
                    }
                });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });

        dialog.show();
    }

    public void showNewCustomerDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater infalter = LayoutInflater.from(getActivity());
        final View view = infalter.inflate(R.layout.dialog_new_customer,null);

        final EditText textNewName = (EditText) view.findViewById(R.id.text_new_name);
        final EditText textNewPhone = (EditText) view.findViewById(R.id.text_new_phone);
        final EditText textNewAddress = (EditText) view.findViewById(R.id.text_new_address);
        final Spinner spinNewPrior = (Spinner) view.findViewById(R.id.spinner_new_priority);
        //final EditText textNewPriorNum = (EditText) view.findViewById(R.id.text_new_priority_number);
        //textNewPriorNum.setEnabled(false);
        spinNewPrior.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 1){
                    //textNewPriorNum.setEnabled(true);
                }else{
                    //textNewPriorNum.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setView(view)
                .setPositiveButton("Add Customer",null)
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean prior = spinNewPrior.getSelectedItemPosition() == 1;
                        String priorNum = "0";
                        if(prior){
                            //priorNum = textNewPriorNum.getText().toString();
                        }
                        if(hasText(textNewName) && hasText(textNewAddress) && hasText(textNewPhone)){
                            new AddCustomerTask().execute(
                                    textNewName.getText().toString(),
                                    textNewPhone.getText().toString(),
                                    textNewAddress.getText().toString(),
                                    String.valueOf(prior),
                                    priorNum
                            );
                        }
                    }
                });

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });

        dialog.show();


    }

    public void refreshList(){
        if (currFilter){
            viewList = new ArrayList<>();
            for (Customers customer : list){
                if(customer.isPriority())
                    viewList.add(customer);
            }
        }else{
            viewList = new ArrayList<>(list);
        }
        adapter = new CustomersAdapter(
                getContext(),
                R.layout.row_item_customers,
                viewList
        );
        listViewItems.setAdapter(adapter);
    }

    /*public void setList(){
        list.add(new Customers(1, "Tommy", "085998001725", "Modernland", false, 0, 0));
        list.add(new Customers(2, "Harvei", "085998001725", "Modernland", false, 0, 0));
        list.add(new Customers(3, "Aldo", "085998001725", "Modernland", true, 1, 11000));
    }*/

    private class AddCustomerTask extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Add Data..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "AddCustomer Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "token=token";
            param = param + "&user="+pref.getString("user_id","");
            param = param + "&customer_name="+params[0];
            param = param + "&customer_phone="+params[1];
            param = param + "&customer_address="+params[2];
            param = param + "&customer_priority="+params[3];
            param = param + "&customer_priority_no="+params[4];
            //Log.v("PARAM : ",param);

            WebService webService = null;
            try {
                webService = new WebService("addCustomer", "POST", param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            String result = "No Data";
            try {
                JSONObject obj = new JSONObject(jsonString);
                String success = obj.getString("status");
                if(success.equals("1")) {
                    String id = obj.getString("message");
                    cust = new Customers(
                            Integer.parseInt(id),
                            params[0],
                            params[1],
                            params[2],
                            Boolean.parseBoolean(params[3]),
                            Integer.parseInt(params[4]),
                            0
                    );
                    result = "New customer has been added";
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            list.add(cust);
            refreshList();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
        }
    }

    public void setList(ArrayList<Customers> res){
        for(Customers c : res){
            list.add(c);
        }
    }

    private class GetCustomerTask extends AsyncTask<String, Void, ArrayList<Customers>>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Get Data..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "GetCustomer Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected ArrayList<Customers> doInBackground(String... params) {
            String param = "token=token";
            param = param + "&user="+pref.getString("user_id","");
            WebService webService = null;
            try {
                webService = new WebService("getCustomer", "POST", param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            ArrayList<Customers> customerList = new ArrayList<>();
            try{
                JSONObject obj = new JSONObject(jsonString);
                String success = obj.getString("status");
                String message = obj.getString("message");
                if(success.equals("1")){
                    JSONArray customerArray = new JSONArray(message);
                    for(int i = 0; i < customerArray.length(); i++){
                        JSONObject customerObject = customerArray.getJSONObject(i);
                        int id = customerObject.getInt("customer_id");
                        String name = customerObject.getString("customer_name");
                        String phone = customerObject.getString("customer_phone");
                        String address = customerObject.getString("customer_address");
                        boolean prior = customerObject.getBoolean("customer_priority");
                        int priorNum = customerObject.getInt("customer_priority_no");
                        int credit = customerObject.getInt("customer_credit");
                        customerList.add(new Customers(id, name, phone, address, prior, priorNum, credit));
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return customerList;
        }

        @Override
        protected void onPostExecute(ArrayList<Customers> res) {
            super.onPostExecute(res);
            if(!res.isEmpty()){
                list.clear();
                setList(res);
                adapter = new CustomersAdapter(
                        context,
                        R.layout.row_item_customers,
                        list
                );
                listViewItems.setAdapter(adapter);
                viewList = new ArrayList<>(list);
            }
            if (swipeRefreshLayout.isRefreshing())swipeRefreshLayout.setRefreshing(false);
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

    private class EditCustomerTask extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Get Data..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "EditCustomer Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "token=token";
            param = param + "&user="+pref.getString("user_id","");
            param = param + "&customer_id=" + params[0];
            param = param + "&customer_name=" + params[1];
            param = param + "&customer_phone=" + params[2];
            param = param + "&customer_address=" + params[3];
            param = param + "&customer_priority=" + params[4];
            param = param + "&customer_credit=" + params[5];
            Log.v("PARAM",param);
            WebService webService = null;
            try {
                webService = new WebService("editCustomer","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            String result = "";
            String success = "0";
            try{
                JSONObject obj = new JSONObject(jsonString);
                success = obj.getString("status");
                result = obj.getString("message");
            }catch(Exception e){
                e.printStackTrace();
            }

            return success;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.v("STATUS",s);
            if(s.equals("0")){
                Toast.makeText(context, "Failed to Udpate", Toast.LENGTH_SHORT).show();
            }else{
                CustomersFragment customersFragment = new CustomersFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment, customersFragment).commit();
                Toast.makeText(context, "Success to Udpate", Toast.LENGTH_SHORT).show();
            }


            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

    public static boolean hasText(EditText editText) {
        String text = editText.getText().toString().trim();
        editText.setError(null);
        if (text.length() == 0) {
            editText.setError("ISI DONG");
            editText.requestFocus();
            return false;
        }
        return true;
    }
}
