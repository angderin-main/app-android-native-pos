package com.androidkt.lagramma;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Tommy on 01/02/2018.
 */

public class Transaction {
    private int id;
    private String address;
    private String phone;
    private String info;
    private int cashPaid;
    private int diskon;
    private String cashType;
    private String orderStatus;
    private String paymentStatus;
    private String customerName;
    private Date orderDueDate;
    private Date orderDate;
    private String userName;
    private String paymentType;
    private Boolean customerPrior;
    private ArrayList<Item> listItem;

    public Transaction(int id, String address, String phone, String info, int cashPaid, int diskon, String cashType, String orderStatus, String paymentStatus, String customerName, Date orderDueDate, Date orderDate, String userName, String paymentType, Boolean customerPrior, ArrayList<Item> listItem) {
        this.id = id;
        this.address = address;
        this.phone = phone;
        this.info = info;
        this.cashPaid = cashPaid;
        this.diskon = diskon;
        this.cashType = cashType;
        this.orderStatus = orderStatus;
        this.paymentStatus = paymentStatus;
        this.customerName = customerName;
        this.orderDueDate = orderDueDate;
        this.orderDate = orderDate;
        this.userName = userName;
        this.paymentType = paymentType;
        this.customerPrior = customerPrior;
        this.listItem = listItem;
    }

    public Transaction(ArrayList<Item> listItem, String paymentType, int diskon,
        Date orderDueDate, int cashPaid){
        this.paymentType = paymentType;
        this.listItem = listItem;
        this.diskon = diskon;
        this.orderDueDate = orderDueDate;
        this.cashPaid = cashPaid;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public void setCashPaid(int cashPaid) {
        this.cashPaid = cashPaid;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getInfo() {
        return info;
    }

    public int getCashPaid() {
        return cashPaid;
    }

    public int getDiskon() {
        return diskon;
    }

    public String getCashType() {
        return cashType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public String getCustomerName() {
        return customerName;
    }

    public Date getOrderDueDate() {
        return orderDueDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public String getUserName() {
        return userName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public Boolean getCustomerPrior() {
        return customerPrior;
    }

    public ArrayList<Item> getListItem() {
        return listItem;
    }
}
