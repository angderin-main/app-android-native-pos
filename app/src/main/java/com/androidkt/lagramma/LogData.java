package com.androidkt.lagramma;

import java.util.ArrayList;

/**
 * Created by Derin Anggara on 4/6/2018.
 */

public class LogData {
    private String date;
    private ArrayList<Log> logs;

    public LogData(String date, ArrayList<Log> logs) {
        this.date = date;
        this.logs = logs;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<Log> getLogs() {
        return logs;
    }

    public void setLogs(ArrayList<Log> logs) {
        this.logs = logs;
    }
}
