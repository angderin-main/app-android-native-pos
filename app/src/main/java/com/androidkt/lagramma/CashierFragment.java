package com.androidkt.lagramma;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.databinding.ObservableArrayList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class CashierFragment extends Fragment {

    Button buttonProcced, buttonCashRegis, buttonAll, buttonNastar, buttonLapis, buttonPack, buttonOther, buttonDate, buttonTime, buttonAddCustomer;
    Spinner spinnerCashType, spinnerOrderStatus, spinnerPaymentStatus;
    EditText  txtPhone, txtAddress, txtInfo;
    TextView txtDate, txtTime, txtTotalPrice, txtCustomer;
    ToggleButton toggleGP;
    ListView listView, listViewCustomer;
    TableLayout tblOrderItem;

    SharedPreferences pref;

    Context context;
    Activity activity;
    StocksAdapter adapter;
    CustomersAdapter custAdapter;
    ArrayList<Item> list = new ArrayList<Item>();
    ArrayList<Item> viewList;
    ArrayList<Item> orderItemList = new ArrayList<>();
    int orderID = 0;
    double totalPrice = 0;
    String itemFilter = "", cashType = "Pre Order", orderInfo = "-";
    Calendar calSet = Calendar.getInstance();
    Customers selectedCust = null;
    View prefView;
    final DecimalFormat decimalFormat = new DecimalFormat("#,###");
    NumberFormat nf = new DecimalFormat("#.####");
    double[] cashierCash = {0,0};

    ArrayList<Integer> disableSelectedOrder = new ArrayList<Integer>();
    ArrayList<DisableView> disableSelectedView = new ArrayList<DisableView>();
    View lastedClickedView;
    int lastedClickedId;

    public CashierFragment() {
        // Required empty public constructor
    }

    private class DisableView{
        public Integer getDisableOrderId() {
            return disableOrderId;
        }

        public void setDisableOrderId(Integer disableOrderId) {
            this.disableOrderId = disableOrderId;
        }

        public View getDisableOrderView() {
            return disableOrderView;
        }

        public void setDisableOrderView(View disableOrderView) {
            this.disableOrderView = disableOrderView;
        }

        public Integer disableOrderId;
        public View disableOrderView;

        public DisableView(Integer disableOrderId, View disableOrderView) {
            this.disableOrderId = disableOrderId;
            this.disableOrderView = disableOrderView;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_cashier, container, false);
        context = getContext();
        activity = getActivity();

        pref = getActivity().getSharedPreferences("LAGRAMMA", MODE_PRIVATE);

        TextView bannerTitle = (TextView) view.findViewById(R.id.banner_title);
        Log.e("ASSETS",getContext().getApplicationContext().getAssets().toString());
        Typeface monserrat = Typeface.createFromAsset(getContext().getApplicationContext().getAssets(),  "fonts/montserrat_light.ttf");
        bannerTitle.setTypeface(monserrat);


        buttonProcced = (Button) view.findViewById(R.id.btn_proceed);
        buttonCashRegis = (Button) view.findViewById(R.id.button_cash_register);
        buttonAll = (Button) view.findViewById(R.id.btn_filter_stock_all);
        buttonNastar = (Button) view.findViewById(R.id.btn_filter_stock_nastar);
        buttonLapis = (Button) view.findViewById(R.id.btn_filter_stock_lapis);
        buttonPack = (Button) view.findViewById(R.id.btn_filter_stock_pack);
        buttonOther = (Button) view.findViewById(R.id.btn_filter_stock_other);
        buttonDate = (Button) view.findViewById(R.id.btn_order_date);
        buttonTime = (Button) view.findViewById(R.id.btn_order_time);
        buttonAddCustomer = (Button) view.findViewById(R.id.btn_add_customer);
        spinnerCashType = (Spinner) view.findViewById(R.id.spinner_cash_type);
        spinnerOrderStatus = (Spinner) view.findViewById(R.id.spinner_order_status);
        spinnerPaymentStatus = (Spinner) view.findViewById(R.id.spinner_payment_status);
        txtAddress = (EditText) view.findViewById(R.id.txt_order_customer_address);
        txtPhone = (EditText) view.findViewById(R.id.txt_order_customer_phone);
        txtInfo = (EditText) view.findViewById(R.id.txt_order_info);
        txtDate = (TextView) view.findViewById(R.id.txt_order_date);
        txtTime = (TextView) view.findViewById(R.id.txt_order_time);
        txtTotalPrice = (TextView) view.findViewById(R.id.txt_total_price);
        txtCustomer = (TextView) view.findViewById(R.id.txt_order_customer_name);
        toggleGP = (ToggleButton) view.findViewById(R.id.toggle_gp);
        listView = (ListView) view.findViewById(R.id.list_items);
        tblOrderItem = (TableLayout) view.findViewById(R.id.table_detail);

        new GetStockTask().execute();

        buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreen));
        calSet.add(Calendar.HOUR_OF_DAY, 3);
        Date chosenDate = calSet.getTime();
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMM yyyy");
        String dfDateStr = sdfDate.format(chosenDate);
        txtDate.setText(dfDateStr);
        toggleGP.setClickable(false);

        SimpleDateFormat sdfTime = new SimpleDateFormat("HH : mm");
        String sdfTimeStr = sdfTime.format(chosenDate);
        txtTime.setText(sdfTimeStr);

        buttonCashRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetCash().execute();
            }
        });

        buttonAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFilter = "";
                refreshList();
                buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonNastar.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonLapis.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonPack.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonOther.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonLapis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFilter = "lapis legit";
                refreshList();
                buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonNastar.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonLapis.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonPack.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonOther.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonNastar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFilter = "nastar";
                refreshList();
                buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonNastar.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonLapis.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonPack.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonOther.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonPack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFilter = "packaging";
                refreshList();
                buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonNastar.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonLapis.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonPack.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonOther.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonOther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemFilter = "others";
                refreshList();
                buttonAll.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonNastar.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonLapis.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonPack.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonOther.setBackgroundColor(getResources().getColor(R.color.colorGreen));
            }
        });

        buttonProcced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(totalPrice != 0 && selectedCust != null) {
                    if(txtAddress.getText().toString().equals("")){
                        txtAddress.setError("Input this field");
                    }else if(txtPhone.getText().toString().equals("")){
                        txtPhone.setError("Input this field");
                    }else{
                        showProceedDialog();
                    }
                }else{
                    Toast.makeText(context, "Input the item to proceed the order", Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        buttonTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog();
            }
        });

        spinnerCashType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cashType = spinnerCashType.getSelectedItem().toString();
                if(cashType.equals("Pre Order")){
                    spinnerPaymentStatus.setSelection(1);
                    spinnerOrderStatus.setSelection(0);
                    buttonDate.setEnabled(true);
                    buttonTime.setEnabled(true);
                }
                else if(cashType.equals("Package")){
                    spinnerPaymentStatus.setSelection(2);
                    spinnerOrderStatus.setSelection(0);
                    buttonDate.setEnabled(true);
                    buttonTime.setEnabled(true);
                }else{
                    spinnerPaymentStatus.setSelection(2);
                    spinnerOrderStatus.setSelection(3);
                    buttonDate.setEnabled(false);
                    buttonTime.setEnabled(false);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerPaymentStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String paymentStatus = spinnerPaymentStatus.getSelectedItem().toString();
                if(paymentStatus.equals("Unpaid") || paymentStatus.equals("Downpaid")){
                    String orderStatus = spinnerOrderStatus.getSelectedItem().toString();
                    if(orderStatus.equals("Finished")){
                        spinnerOrderStatus.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerOrderStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String orderStatus = spinnerOrderStatus.getSelectedItem().toString();
                if(orderStatus.equals("Finished")){
                    String paymentStatus = spinnerPaymentStatus.getSelectedItem().toString();
                    if(paymentStatus.equals("Unpaid") || paymentStatus.equals("Downpaid")){
                        spinnerOrderStatus.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonAddCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNewCustomerDialog();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Item item = viewList.get(position);
                /*lastedClickedView = view;
                if(!disableSelectedOrder.contains(new Integer((int)item.getItemId()))){
                    showConfirmDialog(item, id);
                    disableSelectedOrder.add(item.getItemId());
                    view.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                }
                else {
                    view.setBackgroundColor(getResources().getColor(R.color.colorGray));
                }
                Log.v("Data",disableSelectedOrder.toString());*/
                /****************************************
                *****************************************
                        DISINI SEDANG DIPERBAIKI
                *****************************************
                *****************************************/
                boolean flagDisableView = false;
                for(DisableView dv: disableSelectedView){
                    if (dv.getDisableOrderId() == item.getItemId()){
                        flagDisableView = true;
                    }
                }
                if(!flagDisableView){
                    lastedClickedId = item.getItemId();
                    lastedClickedView = view;
                    showConfirmDialog(item,id);
                }
            }
        });

        txtCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new showListCustomerDialog().execute();
            }
        });

        return view;
    }

    private void releaseView(){
        lastedClickedView.setBackgroundColor(getResources().getColor(R.color.colorWhite));
    }
    private void lockView(){
        disableSelectedView.add(new DisableView(lastedClickedId,lastedClickedView));
        lastedClickedView.setBackgroundColor(getResources().getColor(R.color.colorGray));
    }
    private void releaseViewId(int idItem){
        DisableView temp = null;
        for(DisableView dv: disableSelectedView){
            if (dv.getDisableOrderId() == idItem){
                temp = dv;
                dv.getDisableOrderView().setBackgroundColor(getResources().getColor(R.color.colorWhite));
                break;
            }
        }
        if(temp!=null){
            disableSelectedView.remove(temp);
        }
    }

    private void refreshList(){
        viewList = new ArrayList<>();
        if(!itemFilter.equals("")){
            for(Item item : list){
                if(item.getItemCategory().equals(itemFilter)){
                    viewList.add(item);
                }
            }
        }else{
            viewList = list;
        }
        adapter = new StocksAdapter(
                context,
                R.layout.row_item_stocks,
                viewList
        );
        listView.setAdapter(adapter);

    }

    public void showProceedDialog(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater infalter = LayoutInflater.from(getActivity());
        final View view = infalter.inflate(R.layout.dialog_finish_transaction,null);
        final double[] currPayment = {0};
        final double[] disc = {0};
        final String[] cashPaid = {"0"};

        TextView txtTotal = (TextView) view.findViewById(R.id.txt_dialog_total);
        final Spinner spinnerPayType = (Spinner) view.findViewById(R.id.spinner_dialog_cash);
        final EditText txtCurrPayment = (EditText) view.findViewById(R.id.txt_current_payment);
        final TextView txtRemains = (TextView) view.findViewById(R.id.txt_remain);
        final CheckBox cbDiscount = (CheckBox) view.findViewById(R.id.cb_discount);
        final EditText txtDiscount = (EditText) view.findViewById(R.id.txt_discount);
        final TextView txtExchange = (TextView) view.findViewById(R.id.txt_exchange);

        txtDiscount.setEnabled(false);
        Log.v("CASH TYPE",cashType);

        txtTotal.setText(decimalFormat.format(totalPrice));

//        for(int i = 0; i < Global.CashTypeList.size()-1; i++){
//            if(Global.CashTypeList.get(i).getName().equals(cashType.toLowerCase())){
//                spinnerPayType.setSelection(i);
//            }
//        }
        //spinnerPayType.setEnabled(false);

        final String max = Integer.toString((int) totalPrice);
        Log.v("MAX",max);
        String paymentStatus = spinnerPaymentStatus.getSelectedItem().toString();
        if(paymentStatus.equals("Paid")){
            txtCurrPayment.setText(nf.format(totalPrice));
            currPayment[0] = Double.parseDouble(txtCurrPayment.getText().toString());
            txtRemains.setText("0");
        }else if(paymentStatus.equals("Downpaid")){
            txtCurrPayment.setText(nf.format(totalPrice/2));
            currPayment[0] = Double.parseDouble(txtCurrPayment.getText().toString());
            txtRemains.setText(decimalFormat.format(totalPrice/2));
        }else{
            txtCurrPayment.setText("0");
            currPayment[0] = Double.parseDouble(txtCurrPayment.getText().toString());
            txtRemains.setText(decimalFormat.format(totalPrice));
        }
        //txtCurrPayment.setFilters(new InputFilter[]{new InputFilterMinMax("0",max)});

//        spinnerPayType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if(spinnerPayType.getSelectedItem().toString().equals("Pre Order")){
//                    txtCurrPayment.setText("0");
//                }
//                cashType = spinnerPayType.getSelectedItem().toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        txtDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                double remain = 0;
                if(s.length() != 0){
                    disc[0] = Double.parseDouble(s.toString());
                }else{
                    disc[0] = 0;
                }
                if(currPayment[0] <= totalPrice){
                    remain = (totalPrice - disc[0]) - currPayment[0];
                }
                txtRemains.setText(decimalFormat.format(remain));
                //int maks = (int) (totalPrice - disc[0]);
                //txtCurrPayment.setFilters(new InputFilter[]{new InputFilterMinMax("0",Integer.toString(maks))});
            }
        });

        cbDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbDiscount.isChecked()){
                    int maks = (int)(totalPrice - currPayment[0]);
                    txtDiscount.setFilters(new InputFilter[]{new InputFilterMinMax("0",Integer.toString(maks))});
                    txtDiscount.setEnabled(true);
                }else{
                    txtDiscount.setText("0");
                    txtDiscount.setFilters(new InputFilter[]{new InputFilterMinMax("0",max)});
                    txtDiscount.setEnabled(false);
                }
            }
        });

        txtCurrPayment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() != 0){
                    currPayment[0] = Double.parseDouble(s.toString());
                    double remain = 0;
                    double exchange = 0;
                    if(currPayment[0] <= totalPrice){
                        remain = (totalPrice - disc[0]) - currPayment[0];
                    }else{
                        exchange = Math.abs((totalPrice - disc[0]) - currPayment[0]);
                    }
                    txtRemains.setText(decimalFormat.format(remain));
                    txtExchange.setText(decimalFormat.format(exchange));
                }else{
                    txtRemains.setText(decimalFormat.format(totalPrice));
                    txtCurrPayment.setText("0");
                }
            }

        });

        builder.setView(view)
                .setPositiveButton("Save & Print",
                        null)
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int discount = 0;

                        if(txtInfo.length() != 0){
                            orderInfo = txtInfo.getText().toString();
                        }

                        Date date = calSet.getTime();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String cashTypeId = "", orderStatusId = "", transStatusId = "", orderDueDate = sdf.format(date);

                        for(Option opt:Global.CashTypeList){
                            if(opt.getName().equals(cashType.toLowerCase())){
                                cashTypeId = Integer.toString(opt.getId());
                            }
                        }

                        if(cashTypeId.equals("2") && spinnerPaymentStatus.getSelectedItem().toString().toLowerCase().equals("unpaid")){
                            if(totalPrice - Double.parseDouble(txtCurrPayment.getText().toString()) > 0){
                                transStatusId = "2";
                            }else{
                                transStatusId = "3";
                            }
                        }else{
                            for(Option opt:Global.PaymentStatusList){
                                if(opt.getName().equals(spinnerPaymentStatus.getSelectedItem().toString().toLowerCase())){
                                    transStatusId = Integer.toString(opt.getId());
                                }
                            }
                        }

                        for(Option opt:Global.OrderStatusList){
                            if(opt.getName().equals(spinnerOrderStatus.getSelectedItem().toString().toLowerCase())){
                                orderStatusId = Integer.toString(opt.getId());
                            }
                        }

                        if(Double.parseDouble(txtCurrPayment.getText().toString()) >= totalPrice){
                            cashPaid[0] = nf.format(totalPrice);
                        }else{
                            cashPaid[0] = nf.format(currPayment[0]);
                        }

                        if(cbDiscount.isChecked()){
                            if(txtDiscount.length() != 0){
                                discount = Integer.parseInt(txtDiscount.getText().toString());
                            }else{
                                txtCurrPayment.setError("Input this discount");
                            }
                        }

//                        if(spinnerPayType.getSelectedItem().toString().equals("Pre Order")){
//                            String userid = pref.getString("user_id",null);
//                            new AddOrderTransaction().execute(
//                                    cashPaid[0],
//                                    Integer.toString(discount),
//                                    cashTypeId,
//                                    orderStatusId,
//                                    transStatusId,
//                                    orderDueDate,
//                                    userid
//                            );
//                            dialog.dismiss();
//                        }else{
//                        if(txtAddress.getText().equals("") ){
//                            txtAddress.setError("Input this field");
//                        }else if(txtPhone.getText().equals("")){
//                            txtPhone.setError("Input this field");
//                        }else{
                            if(!txtCurrPayment.getText().toString().equals("0")){
                                ArrayList<ModelPembayaran> modelPembayaran = new ArrayList<ModelPembayaran>();
                                for(Item  ol: orderItemList) {
                                    modelPembayaran.add(new ModelPembayaran(ol.getItemName(), ol.getItemStocks(), ol.getItemPrice()));
                                }

                                if(txtDiscount.getText().toString().length()==0){
                                    txtDiscount.setText("0");
                                }

                                Transaction transaction = new Transaction(
                                        orderItemList,
                                        spinnerPayType.getSelectedItem().toString(),
                                        Integer.parseInt(txtDiscount.getText().toString()),
                                        date,
                                        Integer.parseInt(txtCurrPayment.getText().toString())
                                );
                                String cashierName = pref.getString("username",null);
                                String userid = pref.getString("user_id",null);

                                if(Global.printerStatus) {
                                    Global.printModel.addDataPrint(transaction, modelPembayaran, cashierName, selectedCust);
                                    Global.printModel.print();
                                }
                                else Toast.makeText(context, "Please connect a printer, you can print again from transaction menu", Toast.LENGTH_SHORT).show();

                                new AddOrderTransaction().execute(
                                        txtCurrPayment.getText().toString(),
                                        Integer.toString(discount),
                                        cashTypeId,
                                        orderStatusId,
                                        transStatusId,
                                        orderDueDate,
                                        userid,
                                        spinnerPayType.getSelectedItem().toString()
                                );
                                dialog.dismiss();
                            }else{
                                txtCurrPayment.setError("Input this field");
                            }
                        //}
                        //}
                    }
                });

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });
        dialog.show();
    }

    public void showDateDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View view = inflater.inflate(R.layout.dialog_order_date,null);
        final DatePicker datePicker = (DatePicker) view.findViewById(R.id.dp_order_date);
        datePicker.updateDate(calSet.get(Calendar.YEAR), calSet.get(Calendar.MONTH), calSet.get(Calendar.DATE));

        builder.setView(view).setPositiveButton("Set Date",
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    int day = datePicker.getDayOfMonth();
                    int month = datePicker.getMonth();
                    int year = datePicker.getYear();
                    calSet.set(year, month, day);
                    Date chosenDate = calSet.getTime();
                    SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMM yyyy");
                    String dfDateStr = sdfDate.format(chosenDate);
                    txtDate.setText(dfDateStr);
                }
            })
            .setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
            });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });

        dialog.show();
    }

    public void showTimeDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View view = inflater.inflate(R.layout.dialog_order_time,null);
        final TimePicker timePicker = (TimePicker) view.findViewById(R.id.tp_order_time);
        timePicker.setIs24HourView(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker.setHour(calSet.get(Calendar.HOUR_OF_DAY));
            timePicker.setMinute(calSet.get(Calendar.MINUTE));
        }
        builder.setView(view).setPositiveButton("Set Time",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int hour = 0;
                        int minute = 0;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            hour = timePicker.getHour();
                            minute = timePicker.getMinute();
                        }
                        Log.v("HOUR",Integer.toString(hour));
                        calSet.set(Calendar.HOUR_OF_DAY, hour);
                        calSet.set(Calendar.MINUTE, minute);
                        Date chosenTime = calSet.getTime();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH : mm");
                        String sdfTime = simpleDateFormat.format(chosenTime);
                        txtTime.setText(sdfTime);
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });
        dialog.show();
    }

    public void showNewCustomerDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater infalter = LayoutInflater.from(getActivity());
        final View view = infalter.inflate(R.layout.dialog_new_customer,null);

        final EditText textNewName = (EditText) view.findViewById(R.id.text_new_name);
        final EditText textNewPhone = (EditText) view.findViewById(R.id.text_new_phone);
        final EditText textNewAddress = (EditText) view.findViewById(R.id.text_new_address);
        final Spinner spinNewPrior = (Spinner) view.findViewById(R.id.spinner_new_priority);
        //final EditText textNewPriorNum = (EditText) view.findViewById(R.id.text_new_priority_number);
        //textNewPriorNum.setEnabled(false);
        pref = getActivity().getSharedPreferences("LAGRAMMA", MODE_PRIVATE);
        if(!pref.getString("privilege_id",null).equals("4")){
            spinNewPrior.setVisibility(View.INVISIBLE);
            //textNewPriorNum.setVisibility(View.INVISIBLE);
        }
        spinNewPrior.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 1){
                    //textNewPriorNum.setEnabled(true);
                }else{
                    //textNewPriorNum.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setView(view)
                .setPositiveButton("Add Customer",
                        null
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!textNewName.getText().toString().equals("") && !textNewPhone.getText().toString().equals("") && !textNewAddress.getText().toString().equals("")){
                            if(spinNewPrior.getSelectedItemPosition() == 1 ){//&& textNewPriorNum.getText().toString().equals("")){
                                Toast.makeText(context, "Please insert the Field Prioriity Number at the Form", Toast.LENGTH_SHORT).show();
                            }else{
                                boolean prior = spinNewPrior.getSelectedItemPosition() == 1;
                                String priorNum = "0";
                                if(prior){
                                    //priorNum = textNewPriorNum.getText().toString();
                                }

                                String cashierName = pref.getString("username",null);
                                String userid = pref.getString("user_id",null);

                                new AddCustomerTask().execute(
                                        textNewName.getText().toString(),
                                        textNewPhone.getText().toString(),
                                        textNewAddress.getText().toString(),
                                        String.valueOf(prior),
                                        priorNum,
                                        userid
                                );
                                dialog.dismiss();
                            }
                        }else{
                            Toast.makeText(context, "Please insert the blank fields at the Form", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });
        dialog.show();
    }

    private void showConfirmDialog(final Item item, final long id){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater infalter = LayoutInflater.from(getActivity());
        final View view = infalter.inflate(R.layout.dialog_add_order_item,null);

        final TextView txtItemName = (TextView) view.findViewById(R.id.txt_item_name);
        final NumberPicker npItemQty = (NumberPicker) view.findViewById(R.id.np_item_qty);
        final TextView txtSubTotal = (TextView) view.findViewById(R.id.txt_subtotal_price);


        txtItemName.setText(item.getItemName());
        npItemQty.setMinValue(1);
        npItemQty.setMaxValue(item.getItemStocks());
        txtSubTotal.setText(decimalFormat.format(item.getItemPrice()));

        npItemQty.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                double subTotal = (double) picker.getValue() * item.getItemPrice();
                txtSubTotal.setText(decimalFormat.format(subTotal));
            }
        });

        builder.setView(view)
            .setPositiveButton(
                "Add Item",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        final TableRow row = (TableRow) LayoutInflater.from(getActivity()).inflate(R.layout.row_table_order, null);
                        TextView txtOrderName = (TextView) row.findViewById(R.id.txt_table_order_name);
                        TextView txtOrderStock = (TextView) row.findViewById(R.id.txt_table_order_stock);
                        final TextView txtOrderPrice = (TextView) row.findViewById(R.id.txt_table_order_price);
                        ImageButton btnDelete = (ImageButton) row.findViewById(R.id.btn_delete);

                        double subTotal = npItemQty.getValue() * item.getItemPrice();
                        totalPrice += subTotal;
                        txtTotalPrice.setText(decimalFormat.format(totalPrice));
                        txtOrderPrice.setText(decimalFormat.format(subTotal));
                        Log.v("STOCK",Integer.toString(npItemQty.getValue()));
                        final Item x = new Item(item.getItemId(), item.getItemName(), (int) subTotal, npItemQty.getValue());
                        orderItemList.add(x);

                        btnDelete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                orderItemList.remove(new Item(item.getItemId(), item.getItemName(), npItemQty.getValue() * item.getItemPrice(), npItemQty.getValue()));
                                tblOrderItem.removeView(row);
                                totalPrice -= Double.parseDouble(txtOrderPrice.getText().toString().replace(",",""));
                                orderItemList.remove(x);
                                txtTotalPrice.setText(decimalFormat.format(totalPrice));
                                releaseViewId(item.getItemId());
                                //releaseLock(item.getItemId());
                                //listView.getChildAt((int)id).setEnabled(true);
                            }
                        });
                        txtOrderName.setText(item.getItemName());
                        txtOrderStock.setText(Integer.toString(npItemQty.getValue()));
                        tblOrderItem.addView(row);
                        /****************************************
                         *****************************************
                         DISINI SEDANG DIPERBAIKI
                         *****************************************
                         *****************************************/
                        lockView();
                    }
                })
            .setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        /****************************************
                         *****************************************
                         DISINI SEDANG DIPERBAIKI
                         *****************************************
                         *****************************************/
                        releaseView();
                    }
                });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });
        dialog.show();
    }

    public void showCashRegisDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater infalter = LayoutInflater.from(getActivity());
        final View view = infalter.inflate(R.layout.dialog_cash_register,null);


        final Spinner spinnerCashRegisType = (Spinner) view.findViewById(R.id.spinner_dialog_cash);
        final TextView txtCashierCash = (TextView) view.findViewById(R.id.txt_cashier_cash);
        final EditText txtWithdrawCash = (EditText) view.findViewById(R.id.txt_withdraw_cash);
        final TextView txtRemain = (TextView) view.findViewById(R.id.txt_remain_cash);


        txtCashierCash.setText(decimalFormat.format(cashierCash[0]));
        txtRemain.setText(decimalFormat.format(cashierCash[0]));
        String max = nf.format(cashierCash[0]);
        Log.v("CASHIER CASH",max);

        txtWithdrawCash.setText("0");
        txtWithdrawCash.setFilters(new InputFilter[]{new InputFilterMinMax("0",max)});

        spinnerCashRegisType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spinnerCashRegisType.getSelectedItem().toString().equals("Cash")){
                    txtCashierCash.setText(decimalFormat.format(cashierCash[0]));
                    txtRemain.setText(decimalFormat.format(cashierCash[0]));
                    String max = nf.format(cashierCash[0]);
                    txtWithdrawCash.setText("0");
                    txtWithdrawCash.setFilters(new InputFilter[]{new InputFilterMinMax("0",max)});
                }else{
                    txtCashierCash.setText(decimalFormat.format(cashierCash[1]));
                    txtRemain.setText(decimalFormat.format(cashierCash[1]));
                    String max = nf.format(cashierCash[1]);
                    txtWithdrawCash.setText("0");
                    txtWithdrawCash.setFilters(new InputFilter[]{new InputFilterMinMax("0",max)});
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txtWithdrawCash.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(spinnerCashRegisType.getSelectedItem().toString().equals("Cash")){
                    if(s.length()!= 0){
                        double withdrawCash = Double.parseDouble(txtWithdrawCash.getText().toString());
                        if(withdrawCash >= cashierCash[0]){
                            txtRemain.setText("0");
                        }else{
                            txtRemain.setText(decimalFormat.format(cashierCash[0] - withdrawCash));
                        }
                    }else{
                        txtRemain.setText(decimalFormat.format(cashierCash[0]));
                    }
                }else{
                    if(s.length()!= 0){
                        double withdrawCash = Double.parseDouble(txtWithdrawCash.getText().toString());
                        if(withdrawCash >= cashierCash[1]){
                            txtRemain.setText("0");
                        }else{
                            txtRemain.setText(decimalFormat.format(cashierCash[1] - withdrawCash));
                        }
                    }else{
                        txtRemain.setText(decimalFormat.format(cashierCash[1]));
                    }
                }

            }
        });

        final String cashierName = pref.getString("username",null);
        final String userid = pref.getString("user_id",null);

        builder.setView(view)
                .setPositiveButton(
                        "Withdraw",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new WithdrawCash().execute(
                                        txtWithdrawCash.getText().toString(),
                                        userid,
                                        spinnerCashRegisType.getSelectedItem().toString()
                                );
                            }
                        })
                .setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });
        dialog.show();


    }

    class showListCustomerDialog extends AsyncTask<String, Void, String>{
        ArrayList<Customers> custList = new ArrayList<>();
        ProgressDialog progressDialog;
        AlertDialog.Builder builder;
        LayoutInflater inflater;
        View view;
        Button btnAll, btnGold;
        EditText customerSearch;
        ArrayList<Customers> sortList;
        Customers tempCust;
        boolean goldFilter = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            builder = new AlertDialog.Builder(activity);
            inflater = LayoutInflater.from(activity);
            view = inflater.inflate(R.layout.dialog_list_user,null);
            listViewCustomer = (ListView) view.findViewById(R.id.list_view_order_customer);
            btnAll = (Button) view.findViewById(R.id.btn_sort_all);
            btnGold = (Button) view.findViewById(R.id.btn_sort_gold);
            customerSearch = (EditText) view.findViewById(R.id.text_customer_search);

            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Get Data..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "GetCustomer Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "token=token";
            WebService webService = null;
            try {
                webService = new WebService("getCustomer", "POST", param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            String success = "0";
            try{
                JSONObject obj = new JSONObject(jsonString);
                success = obj.getString("status");
                String message = obj.getString("message");
                if(success.equals("1")){
                    JSONArray customerArray = new JSONArray(message);
                    for(int i = 0; i < customerArray.length(); i++){
                        JSONObject customerObject = customerArray.getJSONObject(i);
                        int id = customerObject.getInt("customer_id");
                        String name = customerObject.getString("customer_name");
                        String phone = customerObject.getString("customer_phone");
                        String address = customerObject.getString("customer_address");
                        boolean prior = customerObject.getBoolean("customer_priority");
                        int priorNum = customerObject.getInt("customer_priority_no");
                        int credit = customerObject.getInt("customer_credit");
                        custList.add(new Customers(id, name, phone, address, prior, priorNum, credit));
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return success;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equals("0")){
                builder.setTitle("Network Connection Problem");
                builder.setMessage("there is a problem between connection this device and server, please try again");
                builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }else{

                custAdapter = new CustomersAdapter(
                        getContext(),
                        R.layout.row_item_customers,
                        custList
                );
                sortList = new ArrayList<>(custList);

                listViewCustomer.setAdapter(custAdapter);

                listViewCustomer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        tempCust = sortList.get(position);
                        Log.v("SELECTED",tempCust.getName());

                        if(prefView!= null)
                            prefView.setBackgroundColor(getResources().getColor(R.color.colorWhite));

                        view.setBackgroundColor(getResources().getColor(R.color.colorGray));
                        prefView = view;

                    }
                });

                btnAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tempCust = null;
                        ArrayList<Customers> temp = new ArrayList<>(custList);
                        sortList = new ArrayList<Customers>();
                        if (!customerSearch.equals("")){
                            for (Customers x : temp){
                                if (x.getName().contains(customerSearch.getText()))
                                    sortList.add(x);
                            }
                        }
                        custAdapter = new CustomersAdapter(
                                getContext(),
                                R.layout.row_item_customers,
                                sortList
                        );
                        listViewCustomer.setAdapter(custAdapter);
                        btnAll.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                        btnGold.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                        goldFilter = false;
                    }
                });

                btnGold.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tempCust = null;
                        ArrayList<Customers> temp = new ArrayList<>();
                        sortList = new ArrayList<Customers>();
                        for (Customers customer : custList){
                            if(customer.isPriority())
                                temp.add(customer);
                            Log.v("NAME",customer.getName());
                        }
                        if (!customerSearch.equals("")){
                            for (Customers x : temp){
                                if (x.getName().contains(customerSearch.getText()))
                                    sortList.add(x);
                            }
                        }
                        custAdapter = new CustomersAdapter(
                                getContext(),
                                R.layout.row_item_customers,
                                sortList
                        );
                        listViewCustomer.setAdapter(custAdapter);
                        btnGold.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                        btnAll.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                        goldFilter = true;
                    }
                });

                customerSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        ArrayList<Customers> temp = new ArrayList<>();
//                        sortList = new ArrayList<>();
                        sortList.clear();
                        if (goldFilter){
                            for (Customers customer : custList){
                                if(customer.isPriority())
                                    temp.add(customer);
                            }
                        }else{
                            temp.addAll(custList);
                        }

                        for (Customers x : temp){
                            if (x.getName().contains(s))
                                sortList.add(x);
                        }
                        custAdapter = new CustomersAdapter(
                                getContext(),
                                R.layout.row_item_customers,
                                sortList
                        );
                        listViewCustomer.setAdapter(custAdapter);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                builder.setView(view)
                    .setPositiveButton(
                            "Add Customer",
                            null
                           )
                    .setNegativeButton(
                            "Cancel",
                            null
                    );
            }

            final AlertDialog dialog = builder.create();

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface d) {

                    Button posBtn = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    posBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(tempCust != null){
                                selectedCust = tempCust;
                                txtCustomer.setText(selectedCust.getName());
                                txtPhone.setText(selectedCust.getPhone());
                                txtAddress.setText(selectedCust.getAddress());
                                if(selectedCust.isPriority())
                                    toggleGP.setChecked(true);
                                else
                                    toggleGP.setChecked(false);
                                dialog.dismiss();
                            }
                        }
                    });

                    Button nevBtn = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                    nevBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                    dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
                }
            });
            dialog.show();
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

    class GetStockTask extends AsyncTask<String, Void, Integer>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Fetching Stock Data..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "Get Item Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            String param = "token=token";
            WebService webService = null;
            try {
                webService = new WebService("getItem","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            try{
                JSONObject obj = new JSONObject(jsonString);
                String success = obj.getString("status");
                String message = obj.getString("message");
                Log.v("MESSAGE",message);
                if(success.equals("1")){
                    JSONArray itemArray = new JSONArray(message);
                    for(int i = 0; i < itemArray.length(); i++){
                        JSONObject itemObject = itemArray.getJSONObject(i);
                        list.add(new Item(
                                itemObject.getInt("item_id"),
                                itemObject.getString("item_name"),
                                itemObject.getInt("item_price"),
                                itemObject.getInt("item_gold_credit"),
                                itemObject.getInt("item_stock"),
                                itemObject.getString("item_category_name"),
                                itemObject.getInt("item_category_id")
                        ));
                    }
                    return 1;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer res) {
            super.onPostExecute(res);
            if(res == 1){
                refreshList();
            }
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

    class AddCustomerTask extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Adding Data...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "Add Customer Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "token=token";
            param = param + "&user="+params[5];
            param = param + "&customer_name="+params[0];
            param = param + "&customer_phone="+params[1];
            param = param + "&customer_address="+params[2];
            param = param + "&customer_priority="+params[3];
            param = param + "&customer_priority_no="+params[4];
            Log.v("PARAM : ",param);

            WebService webService = null;
            try {
                webService = new WebService("addCustomer", "POST", param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            String result = "No Data";
            try {
                JSONObject obj = new JSONObject(jsonString);
                String success = obj.getString("status");
                if(success.equals("1")) {
                    String id = obj.getString("message");
                    selectedCust = new Customers(
                            Integer.parseInt(id),
                            params[0],
                            params[1],
                            params[2],
                            Boolean.parseBoolean(params[3]),
                            Integer.parseInt(params[4]),
                            0
                    );
                    result = "New customer has been added";
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            txtCustomer.setText(selectedCust.getName());
            txtPhone.setText(selectedCust.getPhone());
            txtAddress.setText(selectedCust.getAddress());
            txtInfo.setText("");
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
        }
    }

    class AddOrderTransaction extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Adding Order...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "Add Order Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String success = "0";
            String param = "token=token";
            param = param + "&user="+params[6];
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("customer_id",selectedCust.getId());
                jsonObject.put("order_address",txtAddress.getText().toString());
                jsonObject.put("order_phone",txtPhone.getText().toString());
                jsonObject.put("order_info",orderInfo);
                jsonObject.put("order_paid",Integer.parseInt(params[0]));
                jsonObject.put("order_diskon",Integer.parseInt(params[1]));
                jsonObject.put("cash_type",Integer.parseInt(params[2]));
                jsonObject.put("order_status",Integer.parseInt(params[3]));
                jsonObject.put("payment_status",Integer.parseInt(params[4]));
                jsonObject.put("order_due_date",params[5]);
                jsonObject.put("order_cashier",params[6]);
                jsonObject.put("payment_type",params[7]);
                JSONArray jsonItem = new JSONArray();
                for(int i = 0; i < orderItemList.size(); i++){
                    JSONObject obj = new JSONObject();
                    obj.put("id",orderItemList.get(i).getItemId());
                    obj.put("stock",orderItemList.get(i).getItemStocks());
                    jsonItem.put(i,obj);
                }
                jsonObject.put("order_item",jsonItem);
                param = param + "&data_order="+jsonObject.toString();
                Log.v("PARAM",param);
                //jsonObject.put("order_info",)

            } catch (JSONException e) {
                e.printStackTrace();
            }
            WebService webService = null;

            try {
                webService = new WebService("addOrderTransaction", "POST", param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }

            String jsonString = webService.responseBody;
            //String result = "Failed to Order";
            Log.v("RESPONSE",jsonString);
            try {
                JSONObject jsonObj = new JSONObject(jsonString);
                success = jsonObj.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return success;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equals("0")){
                Toast.makeText(context, "Failed to Order", Toast.LENGTH_SHORT).show();
            }
            else{
                //setTitle("Cashier");
                CashierFragment cashierFragment = new CashierFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment, cashierFragment).commit();
            }

            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

    class GetCash extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Get Cash...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "token=token";
            String success = "0";
            WebService webService = null;
            try {
                webService = new WebService("getCash","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            try{
                JSONObject obj = new JSONObject(jsonString);
                success = obj.getString("status");
                if(success.equals("1")){
                    JSONObject messageObj = obj.getJSONObject("message");
                    cashierCash[0] = messageObj.getDouble("cash");
                    cashierCash[1] = messageObj.getDouble("transfer");
                    return success;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            return success;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equals("0")){
                Toast.makeText(context, "Failed to Get Data", Toast.LENGTH_SHORT).show();
            }else{
                showCashRegisDialog();
            }
            progressDialog.dismiss();
        }
    }

    class WithdrawCash extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Withdraw Cash...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "token=token";
            param = param + "&user="+params[1];
            param = param + "&cash="+params[0];
            param = param + "&type="+params[2];
            WebService webService = null;
            try {
                webService = new WebService("withdrawCash","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String success = "0";
            String jsonString = webService.responseBody;
            try{
                JSONObject obj = new JSONObject(jsonString);
                success = obj.getString("status");
            }catch(Exception e){
                e.printStackTrace();
            }
            return success;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equals("0")){
                Toast.makeText(context, "Failed to Withdraw Cash", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(context, "Success to Withdraw Cash", Toast.LENGTH_SHORT).show();
            }
            progressDialog.dismiss();
        }
    }
}
