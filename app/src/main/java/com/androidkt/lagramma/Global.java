package com.androidkt.lagramma;

import java.util.List;

/**
 * Created by Tommy on 03/01/2018.
 */

public class Global {
    public static List<Option> PrivilegeList;
    public static List<Option> ItemCategoryList;
    public static List<Option> PaymentStatusList;
    public static List<Option> CashTypeList;
    public static List<Option> OrderStatusList;

    public static boolean printerStatus;

    public static ModelPrint printModel;
}
