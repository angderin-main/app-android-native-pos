package com.androidkt.lagramma;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionsFragment extends Fragment {


    ListView listView;
    Button buttonFinish, buttonEdit, buttonVoid, buttonUpcoming, buttonLastTrans, buttonUnpaid, buttonSortDate, buttonPrint;
    TextView txtOrderDate, txtOrderTime, txtDueDate, txtDueTime, txtCashType, txtOrderId, txtName, txtPhone, txtAddress, txtTotalPrice, txtCashPaid, txtRemain, txtDiscount, txtPaymentStat, txtUsername, txtPaymentType;
    Spinner spinnerOrderStatus;
    TableLayout tblDetail;
    LinearLayout linearLayout;

    SharedPreferences pref;

    int selectedIdx, updatedIdx;
    TransactionsAdapter adapter;
    Context context;
    ArrayList<Transaction> list = new ArrayList<Transaction>();
    ArrayList<Transaction> viewList = new ArrayList<>();
    DecimalFormat decimalFormat = new DecimalFormat("#,###");
    NumberFormat nf = new DecimalFormat("#.####");
    Transaction selectedTrans;
    double totalPay = 0;
    String currFilter = "";
    Date DateFrom, DateTo;
    EditText transactionSearch;

    public TransactionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_transactions, container, false);
        context = getContext();

        listView = (ListView) view.findViewById(R.id.list_order_item);
        buttonFinish = (Button) view.findViewById(R.id.button_finish_transaction);
        buttonEdit = (Button) view.findViewById(R.id.button_edit);
        buttonVoid = (Button) view.findViewById(R.id.button_void);
        buttonUpcoming = (Button) view.findViewById(R.id.btn_upcoming);
        buttonLastTrans = (Button) view.findViewById(R.id.btn_last_trans);
        buttonUnpaid = (Button) view.findViewById(R.id.btn_unpaid);
        buttonSortDate = (Button) view.findViewById(R.id.btn_sort_date);
        txtOrderDate = (TextView) view.findViewById(R.id.txt_order_date);
        txtOrderTime = (TextView) view.findViewById(R.id.txt_order_time);
        txtDueDate = (TextView) view.findViewById(R.id.txt_due_date);
        txtDueTime = (TextView) view.findViewById(R.id.txt_due_time);
        txtCashType = (TextView) view.findViewById(R.id.txt_cash_type);
        txtOrderId = (TextView) view.findViewById(R.id.txt_order_id);
        spinnerOrderStatus = (Spinner) view.findViewById(R.id.spinner_order_status);
        txtName = (TextView) view.findViewById(R.id.txt_order_customer);
        txtPhone = (TextView) view.findViewById(R.id.txt_order_phone);
        txtAddress = (TextView) view.findViewById(R.id.txt_order_address);
        txtTotalPrice = (TextView) view.findViewById(R.id.txt_total_price);
        txtCashPaid = (TextView) view.findViewById(R.id.txt_cash_paid);
        txtRemain = (TextView) view.findViewById(R.id.txt_remain);
        txtPaymentStat = (TextView) view.findViewById(R.id.txt_payment_status);
        txtDiscount = (TextView) view.findViewById(R.id.txt_discount);
        tblDetail = (TableLayout) view.findViewById(R.id.tbl_detail);
        linearLayout = (LinearLayout) view.findViewById(R.id.linear_layout);
        buttonPrint = (Button) view.findViewById(R.id.button_print);
        transactionSearch = (EditText) view.findViewById(R.id.text_transaction_search);
        txtUsername = (TextView) view.findViewById(R.id.txt_user_name);
        txtPaymentType = (TextView) view.findViewById(R.id.txt_payment_type);

        pref = getActivity().getSharedPreferences("LAGRAMMA", MODE_PRIVATE);

//        if(pref.getString("privilege_id",null).equals("2")){
//            linearLayout.setVisibility(View.GONE);
//        }

        spinnerOrderStatus.setEnabled(false);
        buttonEdit.setEnabled(false);
        buttonFinish.setEnabled(false);
        buttonVoid.setEnabled(false);
        buttonPrint.setEnabled(false);

        //AsyncTask Get Transaction
        new GetTransactionTask().execute();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedIdx = position;
                Transaction trans = viewList.get(position);
                selectedTrans = trans;
                setTransactionDetail(trans);
                if(!pref.getString("privilege_id",null).equals("2")){
                    buttonEdit.setEnabled(true);
                    buttonFinish.setEnabled(true);
                    buttonVoid.setEnabled(true);
                    buttonPrint.setEnabled(true);
                    spinnerOrderStatus.setEnabled(true);
                }
            }
        });
        buttonFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFinishTransDialog();
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditDialog();
            }
        });

        buttonVoid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVoidDialog();
            }
        });

        buttonUpcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currFilter = "upcoming";
                refreshList();
                buttonUpcoming.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonLastTrans.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonUnpaid.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonSortDate.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonLastTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currFilter = "lasttrans";
                refreshList();
                buttonUpcoming.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonLastTrans.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonUnpaid.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonSortDate.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonUnpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currFilter = "unpaid";
                refreshList();
                buttonUpcoming.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonLastTrans.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                buttonUnpaid.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                buttonSortDate.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
            }
        });

        buttonPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ModelPembayaran> modelPembayaran = new ArrayList<ModelPembayaran>();
                for(Item  ol: selectedTrans.getListItem()) {
                    modelPembayaran.add(new ModelPembayaran(ol.getItemName(), ol.getItemStocks(), ol.getItemPrice()));
                }
                Customers customers = new Customers(txtName.getText().toString(), txtPhone.getText().toString(), txtAddress.getText().toString(), selectedTrans.getCustomerPrior());
                Log.v("Customer",txtName.getText().toString()+" "+txtPhone.getText().toString()+" "+txtAddress.getText().toString()+" "+Boolean.toString(selectedTrans.getCustomerPrior()));
                printTransaction(selectedTrans,modelPembayaran,txtUsername.getText().toString(),customers);
            }
        });

        buttonSortDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSortDateDialog();
            }
        });

        transactionSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                refreshList();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    public void printTransaction(Transaction transaction, ArrayList<ModelPembayaran> daftarPrints, String cashier, Customers customer){
        if(Global.printerStatus) {
            Global.printModel.addDataPrint(transaction,daftarPrints,cashier,customer);
            Global.printModel.print();
        }
        else Toast.makeText(context, "Please connect a printer, you can print again from transaction menu", Toast.LENGTH_SHORT).show();
    }
    public void showFinishTransDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater infalter = LayoutInflater.from(getActivity());
        final View view = infalter.inflate(R.layout.dialog_finish_transaction,null);
        final double[] cur = {0};

        Spinner spinCashType = (Spinner) view.findViewById(R.id.spinner_dialog_cash);
        final EditText txtCurrent = (EditText) view.findViewById(R.id.txt_current_payment);
        CheckBox cbDiscount = (CheckBox) view.findViewById(R.id.cb_discount);
        TextView txtDiscount = (TextView) view.findViewById(R.id.txt_discount);
        TextView txtCashType = (TextView) view.findViewById(R.id.txt_cash_type);
        final TextView txtRemain = (TextView) view.findViewById(R.id.txt_remain);
        TextView txtTotal = (TextView) view.findViewById(R.id.txt_dialog_total);

        spinCashType.setVisibility(View.GONE);
        cbDiscount.setVisibility(View.GONE);
        txtDiscount.setVisibility(View.GONE);
        txtCashType.setVisibility(View.GONE);
        txtTotal.setText(decimalFormat.format(totalPay));
        txtRemain.setText(decimalFormat.format(totalPay));
        txtCurrent.setText("0");
        //txtCurrent.setFilter(new InputFilter[]{new InputFilterMinMax("0",nf.format(totalPay))});
        txtCurrent.setFilters(new InputFilter[]{new InputFilterMinMax("0",nf.format(totalPay))});

        txtCurrent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(txtCurrent.length() != 0){
                    cur[0] = Double.parseDouble(txtCurrent.getText().toString());
                    txtRemain.setText(decimalFormat.format(totalPay - cur[0]));
                }else{
                    txtRemain.setText(decimalFormat.format(totalPay));
                }
            }
        });

        builder.setView(view)
                .setPositiveButton("Save",
                        null)
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
            Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(txtCurrent.length() == 0){
                        txtCurrent.setError("Input this field");
                    }else{
                        Transaction trans = viewList.get(selectedIdx);
                        String orderId = String.valueOf(trans.getId());
                        String paymentId = "1";
                        String paymentName = "unpaid";
                        if(txtRemain.getText().toString().equals("0") && trans.getPaymentStatus().equals("unpaid")){
                            paymentId = "3";
                            paymentName = "paid";
                        }else{
                            paymentId = "2";
                            paymentName = "downpaid";
                        }

                        new AddCashPaid().execute(
                                orderId,
                                paymentId,
                                txtCurrent.getText().toString(),
                                paymentName
                        );
                    }
                    dialog.dismiss();
                }
            });
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });

        dialog.show();
    }
    public void showVoidDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setTitle("Delete Order");
        builder.setMessage("Make sure your order want to delete is correct");

        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Transaction trans = viewList.get(selectedIdx);
                new DeleteOrder().execute(String.valueOf(trans.getId()));
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });
        dialog.show();
    }
    public void showEditDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setTitle("Edit Confirmation");
        builder.setMessage("Make sure your change is correct");
        builder.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Transaction trans = viewList.get(selectedIdx);
                String orderId = String.valueOf(trans.getId());
                Option option = null;
                for(Option opt : Global.OrderStatusList){
                    if(spinnerOrderStatus.getSelectedItem().toString().toLowerCase().contains(opt.getName())){
                        option = opt;
                    }
                }
                new EditTransactionTask().execute(
                        orderId,
                        String.valueOf(option.getId()),
                        option.getName()
                );
                Log.v("ORDER ID",orderId);
                Log.v("OPTION ID",String.valueOf(option.getId()));
                Log.v("OPTION NAME",option.getName());
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });

        dialog.show();

    }
    public void showSortDateDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater infalter = LayoutInflater.from(getActivity());
        final View view = infalter.inflate(R.layout.dialog_sort_date,null);

        final DatePicker dpFrom = (DatePicker) view.findViewById(R.id.dp_from);
        final DatePicker dpTo = (DatePicker) view.findViewById(R.id.dp_to);

        builder.setView(view)
                .setPositiveButton("Sort",null)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int day = dpFrom.getDayOfMonth();
                        int month = dpFrom.getMonth();
                        int year = dpFrom.getYear();
                        Calendar cal = Calendar.getInstance();
                        cal.set(year, month, day);
                        cal.add(Calendar.DATE, -1);
                        DateFrom = cal.getTime();

                        day = dpTo.getDayOfMonth();
                        month = dpTo.getMonth();
                        year = dpTo.getYear();
                        cal.set(year, month, day);
                        cal.add(Calendar.DATE, 1);
                        DateTo = cal.getTime();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
                        Log.v("DATE",sdf.format(DateFrom)+"TO"+sdf.format(DateTo));
                        if(DateFrom.getTime() > DateTo.getTime()){
                            Toast.makeText(context, "Cannot sort date", Toast.LENGTH_SHORT).show();
                        }else{
                            currFilter = "sortdate";
                            refreshList();
                            buttonUpcoming.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                            buttonLastTrans.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                            buttonUnpaid.setBackgroundColor(getResources().getColor(R.color.colorGreenDark));
                            buttonSortDate.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                            dialog.dismiss();
                        }
                    }
                });
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(Color.parseColor("#0a4a37"));
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setPadding(10, 10, 10, 10);

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#0a4a37"));
            }
        });

        dialog.show();
    }
    public void refreshList(){
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();

//        Log.v("LIST 0",list.get(list.size()-1).getCustomerName());
//        Log.v("LIST SIZE",String.valueOf(list.size()));
//        Log.v("FILTER",currFilter);
        viewList = new ArrayList<>();
        ArrayList<Transaction> temp = new ArrayList<>();

        if (!transactionSearch.getText().equals("")){
            for (Transaction transaction : list){
                if (transaction.getCustomerName().contains(transactionSearch.getText()) ||
                        Integer.toString(transaction.getId()).contains(transactionSearch.getText())||
                        transaction.getPhone().contains(transactionSearch.getText())){
                    temp.add(transaction);
                }
            }
        }

        if(currFilter.equals("upcoming")){
            Log.v("CALENDAR",date.toString());
            for(Transaction trans : temp){
                Log.v("Compare",Integer.toString(trans.getOrderDueDate().compareTo(date)));
                Log.v("DUE DATE",trans.getOrderDueDate().toString());
                if(!trans.getPaymentStatus().equals("paid") && trans.getOrderDueDate().compareTo(date) > 0){
                    viewList.add(trans);
                }
            }
            Collections.sort(viewList, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o2.getOrderDueDate().compareTo(o1.getOrderDueDate());
                }
            });
        }else if(currFilter.equals("lasttrans")){
            Log.v("FILTER","LASTTRANS");
            for(int i = temp.size()-1 ; i >= 0; i--){
                viewList.add(temp.get(i));
            }
        }else if(currFilter.equals("unpaid")){
            for(Transaction trans : temp){
                if(!trans.getPaymentStatus().equals("paid")){
                    viewList.add(trans);
                }
            }
        }
        else if(currFilter.equals("sortdate")){
            for(Transaction trans : temp){
                if(trans.getOrderDate().after(DateFrom) && trans.getOrderDate().before(DateTo)){
                    viewList.add(trans);
                }
            }
        }else{
            viewList = temp;
        }
        adapter = new TransactionsAdapter(
                getContext(),
                R.layout.row_item_orders,
                viewList
        );
        listView.setAdapter(adapter);
    }
    public void setTransactionDetail(Transaction trans){
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH : mm");
        txtCashType.setText(trans.getCashType().toUpperCase());
        txtOrderDate.setText(sdfDate.format(trans.getOrderDate()));
        txtOrderTime.setText(sdfTime.format(trans.getOrderDate()));
        txtDueDate.setText(sdfDate.format(trans.getOrderDueDate()));
        txtDueTime.setText(sdfTime.format(trans.getOrderDueDate()));
        txtOrderId.setText(String.valueOf(trans.getId()));
        txtPaymentType.setText(trans.getPaymentType());
        txtUsername.setText(trans.getUserName());
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).getId() == trans.getId()){
                updatedIdx = i;
            }
        }
        for(int i = 0; i < Global.OrderStatusList.size(); i++){
            if(Global.OrderStatusList.get(i).getName().equals(trans.getOrderStatus())){
                spinnerOrderStatus.setSelection(i);
            }
        }


        txtName.setText(trans.getCustomerName());
        txtPhone.setText(trans.getPhone());
        txtAddress.setText(trans.getAddress());

        tblDetail.removeAllViews();
        TableRow row = (TableRow) LayoutInflater.from(getActivity()).inflate(R.layout.row_table_transaction, null);
        TextView txtTblName = (TextView) row.findViewById(R.id.txt_name);
        TextView txtTblQty = (TextView) row.findViewById(R.id.txt_qty);
        TextView txtTblPrice = (TextView) row.findViewById(R.id.txt_price);
        txtTblName.setText("Name");
        txtTblQty.setText("Qty");
        txtTblPrice.setText("Price");
        double totalPrice = 0;
        tblDetail.addView(row);
        for(Item item : trans.getListItem()){
            TableRow rows = (TableRow) LayoutInflater.from(getActivity()).inflate(R.layout.row_table_transaction, null);
            TextView tvName, tvQty, tvPrice;
            tvName = (TextView) rows.findViewById(R.id.txt_name);
            tvName.setText(item.getItemName());

            Log.v("ITEM STOCK",String.valueOf(item.getItemStocks()));
            tvQty = (TextView) rows.findViewById(R.id.txt_qty);
            tvQty.setText(String.valueOf(item.getItemStocks()));

            Log.v("ITEM PRICE",String.valueOf(item.getItemPrice()));
            tvPrice = (TextView) rows.findViewById(R.id.txt_price);
            double subtotal = (double)(item.getItemStocks() * item.getItemPrice());
            tvPrice.setText(decimalFormat.format(subtotal));
            totalPrice = totalPrice + subtotal;
            tblDetail.addView(rows);
        }
        if(!pref.getString("privilege_id",null).equals("2")) {
            txtPaymentStat.setText(trans.getPaymentStatus().toUpperCase());
            totalPrice = totalPrice - (double) (trans.getDiskon());
            txtDiscount.setText(decimalFormat.format((double) (trans.getDiskon())));
            txtTotalPrice.setText(decimalFormat.format(totalPrice));
            //Log.v("TOTAL PRICE",decimalFormat.format(totalPrice));
            txtCashPaid.setText(decimalFormat.format((double) (trans.getCashPaid())));
            double remain = totalPrice - trans.getCashPaid();
            if (remain < 0) {
                remain = 0;
            }
            totalPay = remain;
            txtRemain.setText(decimalFormat.format(remain));
        }
    }

    class GetTransactionTask extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Get Transaction Data...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "Get Transaction Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String success = "0";
            String param = "token=token";
            param = param + "&user="+pref.getString("user_id","");
            WebService webService = null;

            try {
                webService = new WebService("getOrderTransaction","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                success = jsonObject.getString("status");
                if(success.equals("1")) {
                    JSONArray transJSON = jsonObject.getJSONArray("message");
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    for (int i = 0; i < transJSON.length(); i++) {
                        JSONObject obj = transJSON.getJSONObject(i);
                        Date dueDate = format.parse(obj.getString("order_due_date"));
                        Date orderDate = format.parse(obj.getString("order_date"));
                        JSONArray itemArr = obj.getJSONArray("order_item");
                        ArrayList<Item> item = new ArrayList<>();
                        for (int j = 0; j < itemArr.length(); j++) {
                            JSONObject itemJson = itemArr.getJSONObject(j);
                            item.add(new Item(
                                    itemJson.getInt("item_id"),
                                    itemJson.getString("item_name"),
                                    itemJson.getInt("item_price"),
                                    itemJson.getInt("item_stock")
                                    )
                            );
                        }
                        Transaction tempTrans = new Transaction(
                                obj.getInt("order_id"),
                                obj.getString("order_address"),
                                obj.getString("order_phone"),
                                obj.getString("order_info"),
                                obj.getInt("order_cash_paid"),
                                obj.getInt("order_diskon"),
                                obj.getString("cash_type_name"),
                                obj.getString("order_status_name"),
                                obj.getString("payment_status_name"),
                                obj.getString("customer_name"),
                                dueDate,
                                orderDate,
                                obj.getString("user_name"),
                                obj.getString("payment_type"),
                                obj.getBoolean("customer_priority"),
                                item
                        );
                        list.add(tempTrans);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return success;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equals("1")){
                refreshList();
            }else{
                Toast.makeText(context, "No Data", Toast.LENGTH_SHORT).show();
            }
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

    class EditTransactionTask extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Update Transaction Data...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "Update Transaction Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String success = "0";
            String param = "token=token";
            param = param + "&user="+pref.getString("user_id","");
            param = param + "&order_id="+params[0];
            param = param + "&order_status_id="+params[1];
            Log.v("PARAM",param);
            WebService webService = null;
            try {
                webService = new WebService("editOrderTransaction","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                Toast.makeText(context, "GetItem Timeout", Toast.LENGTH_SHORT).show();
                cancel(true);
            }
            String jsonString = webService.responseBody;

            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                success = jsonObject.getString("status");
                if(success.equals("1")){
                    selectedTrans.setOrderStatus(params[2]);
                    list.set(updatedIdx, selectedTrans);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return success;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            String msg = "Failed to Update Data";
            if(s.equals("1")){
                msg = "Success to Update data";
                refreshList();
            }
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }
    }

    class AddCashPaid extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Update Transaction Data...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "Update Transaction Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "token=token";
            param = param + "&user="+pref.getString("user_id","");
            param = param + "&order_id=" + params[0];
            param = param + "&payment_status=" + params[1];
            param = param + "&cash_paid=" + params[2];
            Log.v("PARAM",param);
            WebService webService = null;

            try {
                webService = new WebService("addCashPaid","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                Toast.makeText(context, "GetItem Timeout", Toast.LENGTH_SHORT).show();
                cancel(true);
            }

            String jsonString = webService.responseBody;
            String success = "0";
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                success = jsonObject.getString("status");
                if(success.equals("1")){
                    selectedTrans.setPaymentStatus(params[3]);
                    selectedTrans.setCashPaid(Integer.parseInt(params[2]));
                    list.set(updatedIdx,selectedTrans);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return success;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            String msg = "Failed to Update Data";
            if(s.equals("1")){
                msg = "Success to Update data";
                refreshList();
                setTransactionDetail(selectedTrans);
            }
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }
    }

    class DeleteOrder extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Delete Transaction Data...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "Delete Transaction Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            String param = "token=token";
            param = param + "&user="+pref.getString("user_id","");
            param = param + "&order_id=" + params[0];
            Log.v("PARAM",param);
            WebService webService = null;
            String success = "0";
            try {
                webService = new WebService("deleteOrder","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                success = jsonObject.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return success;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equals("1")){
                TransactionsFragment transactionsFragment = new TransactionsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.fragment, transactionsFragment).commit();
                progressDialog.dismiss();
            }else{
                Toast.makeText(context, "Failed to delete order", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }
    }
}
