package com.androidkt.lagramma;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

/**
 * Created by Tommy on 14/01/2018.
 */

public class CustomerData {
    private ArrayList<Customers> customerList, list;
    private Context context;
    private ListView listView;

    public CustomerData(){ customerList = new ArrayList<>();}

    public void setCustomerData(Context context, ArrayList<Customers> list, ListView listView){
        customerList.clear();
        this.context = context;
        this.listView = listView;
        new GetCustomer().execute();
    }

    class GetCustomer extends AsyncTask<String, Void, Integer> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Fetch Customer Data");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "GetCustomer Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            String param = "token=token";
            WebService webService = null;
            try {
                webService = new WebService("getCustomer", "POST", param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            String result = "No Data";
            Log.v("RESPONSE",jsonString);
            ArrayList<Customers> customerList = new ArrayList<>();
            try{
                JSONObject obj = new JSONObject(jsonString);
                String success = obj.getString("status");
                String message = obj.getString("message");
                if(success.equals("1")){
                    JSONArray customerArray = new JSONArray(message);
                    for(int i = 0; i < customerArray.length(); i++){
                        JSONObject customerObject = customerArray.getJSONObject(i);
                        int id = customerObject.getInt("customer_id");
                        String name = customerObject.getString("customer_name");
                        String phone = customerObject.getString("customer_phone");
                        String address = customerObject.getString("customer_address");
                        boolean prior = customerObject.getBoolean("customer_priority");
                        int priorNum = customerObject.getInt("customer_priority_no");
                        int credit = customerObject.getInt("customer_credit");
                        customerList.add(new Customers(id, name, phone, address, prior, priorNum, credit));
                    }
                    return 1;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer res) {
            super.onPostExecute(res);
            if(res == 1){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                listView.setAdapter(new CustomersAdapter(
                        context,
                        R.layout.row_item_customers,
                        customerList
                ));
                list = customerList;
            }
            //list = customerses;
            ///Log.v("LIST ",list.get(0).getName());
            //Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();

            progressDialog.hide();
            progressDialog.dismiss();

        }
    }

}
