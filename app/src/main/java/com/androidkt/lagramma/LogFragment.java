package com.androidkt.lagramma;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LogFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Context context;
    private ListView listLogData, listLogDetail;
    private LogDataAdapter logDataAdapter;
    private LogDetailsAdapter logDetailsAdapter;
    private ArrayList<LogData> logData;
    private SharedPreferences pref;
    private SwipeRefreshLayout swipeRefreshLayout;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LogFragment newInstance(String param1, String param2) {
        LogFragment fragment = new LogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_log, container, false);
        context = getContext();

        pref = getContext().getApplicationContext().getSharedPreferences("LAGRAMMA", MODE_PRIVATE);

        listLogData = (ListView) view.findViewById(R.id.list_log_date);
        listLogDetail = (ListView) view.findViewById(R.id.list_log_detail);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);

        logData = new ArrayList<LogData>();

        listLogData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UpdateListLogDetails(logData.get(position).getLogs());
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetLogData().execute();
            }
        });

//        seedData();
        new GetLogData().execute();

        return view;
    }

    public void seedData(){

        ArrayList<Log> logs = new ArrayList<Log>();
        logs.add(new Log(1,"08-04-2018 01:02:03","Derin","Insert stock"));
        logs.add(new Log(2,"08-04-2018 04:05:06","Apek","Update stock"));
        logs.add(new Log(3,"08-04-2018 07:08:09","Tommy","Remove stock"));
        logs.add(new Log(4,"08-04-2018 10:11:12","Aldo","Inser stock"));
        logData.add(new LogData("08-04-2018", logs));

        logs = new ArrayList<Log>();
        logs.add(new Log(1,"09-04-2018 01:02:03","Aldo","Inser stock"));
        logs.add(new Log(2,"09-04-2018 04:05:06","Tommy","Remove stock"));
        logs.add(new Log(3,"09-04-2018 07:08:09","Apek","Update stock"));
        logs.add(new Log(4,"09-04-2018 10:11:12","Derin","Insert stock"));
        logData.add(new LogData("09-04-2018", logs));

        UpdateListData();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class GetLogData extends AsyncTask<String, Void, Integer> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Fetching Stock Data..");
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            progressDialog.hide();
            progressDialog.dismiss();
            Toast.makeText(context, "GetLog Timeout", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            String param = "";
//            String param = "token=token";
//            param = param + "&user="+pref.getString("username","");
            WebService webService = null;
            try {
                webService = new WebService("GetLog","POST",param);
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                cancel(true);
            }
            String jsonString = webService.responseBody;
            try{
                JSONObject obj = new JSONObject(jsonString);
                String success = obj.getString("status");
                String message = obj.getString("message");
                android.util.Log.v("MESSAGE",message);
                if(success.equals("1")){
                    logData.clear();
                    JSONArray itemArray = new JSONArray(message);
                    for(int i = 0; i < itemArray.length(); i++){
                        JSONObject itemObject = itemArray.getJSONObject(i);
                        String date = itemObject.getString("log_date");
                        String logs = itemObject.getString("logs");
                        JSONArray logArray = new JSONArray(logs);
                        ArrayList<Log> logArrayList = new ArrayList<Log>();
                        for (int x = 0; x < logArray.length(); x++){
                            JSONObject logDetail = logArray.getJSONObject(x);
                            logArrayList.add(new Log(
                                    logDetail.getInt("log_id"),
                                    logDetail.getString("log_date"),
                                    logDetail.getString("user_name"),
                                    logDetail.getString("log_desc")
                            ));
                        }
                        logData.add(
                                new LogData(
                                        date,
                                        logArrayList
                                )
                        );
                    }
                    return 1;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer res) {
            super.onPostExecute(res);
            if (swipeRefreshLayout.isRefreshing())swipeRefreshLayout.setRefreshing(false);
            if(res == 1){
                UpdateListData();
            }
            progressDialog.hide();
            progressDialog.dismiss();
        }
    }

    public void UpdateListData(){
        listLogData.setAdapter(new LogDataAdapter(
                context,
                R.layout.row_item_log_data,
                logData
        ));
    }

    public void UpdateListLogDetails(ArrayList<Log> logs){
        listLogDetail.setAdapter(new LogDetailsAdapter(
                context,
                R.layout.row_item_log_detail,
                logs
        ));
    }
}
