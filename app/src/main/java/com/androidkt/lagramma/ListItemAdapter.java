package com.androidkt.lagramma;

import android.content.Context;
import android.databinding.ObservableArrayList;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Derin Anggara on 12/21/2017.
 */

public class ListItemAdapter extends ArrayAdapter<Item> {

    TextView textItemName;
    TextView textItemPrice;
    TextView textItemStock;
    private ArrayList<Item> data;

    public ListItemAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<Item> data) {
        super(context, resource, data);
        this.data = data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;

        if (view == null){
            LayoutInflater layoutInflater;
            layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(R.layout.row_item_items,null);
            textItemName = (TextView) view.findViewById(R.id.text_item_name);
            textItemPrice = (TextView) view.findViewById(R.id.text_item_price);
            textItemStock = (TextView) view.findViewById(R.id.text_item_stock);
        }

        Item item = data.get(position);

        if (item != null){
            textItemName.setText(item.getItemName());
            textItemPrice.setText(Integer.toString(item.getItemPrice()).substring(0,3));
            textItemStock.setText(Integer.toString(item.getItemStocks()));
        }

//        return super.getView(position, convertView, parent);
        return view;
    }
}
